﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CaioCs;
using Contec_Const;

namespace Contec
{
    /// <summary>
    /// Contecのロギング状態を表しています
    /// </summary>
    public enum LoggingState
    {
        /// <summary>
        /// エラーが発生しています
        /// </summary>
        Error = -1,
        /// <summary>
        /// 何もしていない状態です
        /// </summary>
        Standby = 0,
        /// <summary>
        /// ロギング開始命令受信～ロギングスタート前の初期化の状態です。
        /// </summary>
        StartOn,
        /// <summary>
        /// ロギングを行っています
        /// </summary>
        Logging,
        /// <summary>
        /// ロギングを終了し、ログデータを整形しています
        /// </summary>
        DataMaiking
    }

    /// <summary>
    /// ロギングの結果を表します
    /// </summary>
    public enum ResultInfo
    {
        /// <summary>
        /// 異常終了しました(取得データは保証されていません)
        /// </summary>
        Fatal = -2,
        /// <summary>
        /// ロギング上限値に達して終了しました
        /// </summary>
        LogLimitOver = -1,
        /// <summary>
        /// 正常に終了しました
        /// </summary>
        OK = 0,
        /// <summary>
        /// ロギング中です
        /// </summary>
        NoLogging
    }

    /// <summary>
    /// 
    /// </summary>
    public class LoggingToken
    {
        private bool _stopFlg = false;
        /// <summary>
        /// ロギングを停止させます
        /// </summary>
        public void LoggingStop()
        {
            this._stopFlg = true;
        }

        private LoggingState _state = LoggingState.Standby;
        /// <summary>
        /// Contecのロギング状態を取得します
        /// </summary>
        public LoggingState State
        {
            get => this._state;
        }

        private ResultInfo _result = ResultInfo.OK;
        /// <summary>
        /// ロギングの結果を取得します
        /// </summary>
        public ResultInfo Result
        {
            get => this._result;
        }

        private List<List<float>> _Data = new List<List<float>>();
        /// <summary>
        /// ロギングデータを取得します
        /// </summary>
        public List<List<float>> Data
        {
            get => this._Data;
        }
        /// <summary>
        ///  ロギングデータを消去します
        /// </summary>
        public void DataClear()
        {
            this._Data.Clear();
        }
    }

    /// <summary>
    /// Contecロギングクラス
    /// </summary>
    public class Contec_CS
    {
        /// <summary>
        /// デストラクタ処理_イニシャライズ済みのデバイス開放を行う
        /// </summary>
        ~Contec_CS()
        {
            Exit();
        }

        #region Private Field

        private Caio _caio;
        private short _ch_Num;
        private bool _IsInit = false;

        
        private int _safeCount = 5000;
        #endregion

        #region Property

        private int _MaxLoggingCount = 10000;
        /// <summary>
        /// 
        /// </summary>
        public int LoggingCountLimit
        {
            get => this._MaxLoggingCount;
            set => this._MaxLoggingCount = value;
        }

        private int _timeOut = 60;
        public int TimeOut
        {
            get => this._timeOut;
            set => this._timeOut = value;
        }

        private short _id;
        /// <summary>
        /// 設定されているIDを取得します
        /// </summary>
        public short ID
        {
            get => this._id;
        }

        private string _deviceName;
        /// <summary>
        /// 設定されているデバイスネーム
        /// </summary>
        public DeviceName DeviceName
        {
            get => (DeviceName)Enum.Parse(typeof(DeviceName), this._deviceName);
        }

        /// <summary>
        /// 設定可能な最大ロギングCH数を取得します
        /// </summary>
        public short AiMaxChannels
        {
            get
            {
                short maxCh = 0;
                var ret = this._caio.GetAiMaxChannels(this._id, out maxCh);
                if (ret != 0) { throw new Exception(GetError(ret)); }

                return maxCh;
            }
        }

        /// <summary>
        /// 測定レンジの設定、取得を行います
        /// </summary>
        public IO_Range Range
        {
            get
            {
                short range;
                var ret = this._caio.GetAiRange(this._id, 0, out range);
                if (ret != 0) { throw new Exception(GetError(ret)); }
                return (IO_Range)range;

            }
            set
            {
                var ret = this._caio.SetAiRangeAll(this._id, value);
                if (ret != 0) { throw new Exception(GetError(ret)); }
            }
        }

        /// <summary>
        /// 測定ch数の設定、取得を行います
        /// </summary>
        public short CH_Num
        {
            get
            {
                var ret = this._caio.GetAiChannels(this._id, out this._ch_Num);
                if (ret != 0) { throw new Exception(GetError(ret)); }

                return _ch_Num;
            }
            set
            {
                var ret = this._caio.SetAiChannels(this._id, value);
                if (ret != 0) { throw new Exception(GetError(ret)); }
                this._ch_Num = value;
            }
        }

        /// <summary>
        /// MemoryTypeの設定、取得を行います。
        /// </summary>
        public MemoryType MemoryType
        {
            get
            {
                MemoryType type;
                var ret = this._caio.GetAiMemoryType(this._id, out type);
                if (ret != 0) { throw new Exception(GetError(ret)); }
                return type;
            }
            set
            {
                var ret = this._caio.SetAiMemoryType(this._id, value);
                if (ret != 0) { throw new Exception(GetError(ret)); }
            }
        }

        /// <summary>
        /// ClockTypeの設定、取得を行います。
        /// </summary>
        public AI_ClockType ClockType
        {
            get
            {
                AI_ClockType type;
                var ret = this._caio.GetAiClockType(this._id, out type);
                if (ret != 0) { throw new Exception(GetError(ret)); }
                return type;
            }
            set
            {
                var ret = this._caio.SetAiClockType(this._id, value);
                if (ret != 0) { throw new Exception(GetError(ret)); }
            }
        }

        private float _scanRate;
        /// <summary>
        /// スキャンレートの設定、取得を行います [μSec]
        /// </summary>
        public float ScanRate
        {
            get
            {
                var ret = _caio.GetAiSamplingClock(this._id, out float scanrate);
                if (ret != 0)
                {
                    throw new Exception(GetError(ret));
                }
                return scanrate;
            }
            set
            {
                var ret = this._caio.SetAiSamplingClock(this._id, value);
                if (ret != 0)
                {
                    throw new Exception(GetError(ret));
                }
                this._scanRate = value;
            }

        }

        /// <summary>
        /// 測定開始トリガーの設定、取得を行います
        /// </summary>
        public Start_Trigger StartTrigger
        {
            get
            {
                Start_Trigger trigger;
                var ret = this._caio.GetAiStartTrigger(this._id, out trigger);
                if (ret != 0) { throw new Exception(GetError(ret)); }
                return trigger;
            }
            set
            {
                var ret = this._caio.SetAiStartTrigger(this._id, value);
                if (ret != 0) { throw new Exception(GetError(ret)); }
            }
        }

        /// <summary>
        /// 測定終了トリガーの設定、取得を行います
        /// </summary>
        public Stop_Trigger StopTrigger
        {
            get
            {
                Stop_Trigger trigger;
                var ret = this._caio.GetAiStopTrigger(this._id, out trigger);
                if (ret != 0) { throw new Exception(GetError(ret)); }
                return trigger;
            }
            set
            {
                var ret = this._caio.SetAiStopTrigger(this._id, value);
                if (ret != 0) { throw new Exception(GetError(ret)); }
            }
        }

        private bool _stopFlg = false;
        /// <summary>
        /// ロギングを停止させます
        /// </summary>
        public void LoggingStop()
        {
            this._stopFlg = true;
        }

        /// <summary>
        /// ロギングを停止させ、終了処理が完了するまで待ちます
        /// </summary>
        public void LoggingStopAndJoin()
        {
            this._stopFlg = true;
            while (this.State != LoggingState.Standby && this.State != LoggingState.Error)
            {
            }
        }

        private LoggingState _state = LoggingState.Standby;
        /// <summary>
        /// Contecのロギング状態を取得します
        /// </summary>
        public LoggingState State
        {
            get => this._state;
        }

        private ResultInfo _result = ResultInfo.OK;
        /// <summary>
        /// ロギングの結果を取得します
        /// </summary>
        public ResultInfo Result
        {
            get => this._result;
        }

        private List<List<float>> _Data = new List<List<float>>();
        /// <summary>
        /// ロギングデータを取得します
        /// </summary>
        public List<List<float>> Data
        {
            get => this._Data;
        }
        /// <summary>
        ///  ロギングデータを消去します
        /// </summary>
        public void DataClear()
        {
            this._Data.Clear();
        }
        #endregion

        public Contec_CS(DeviceName deviceName, short chNum = 1)
        {
            this._caio = new Caio();
            this._deviceName = Enum.GetName(typeof(DeviceName), deviceName);
            Init(chNum);
        }

        /// <summary>
        /// 初期化処理を行います
        /// </summary>
        public void Init(short ch_Num)
        {
            short id;
            var ret = this._caio.Init(this.DeviceName, out id);
            if (ret != 0) { throw new Exception(GetError(ret)); }
            this._id = id;

            // プロセスリセット
            ret = this._caio.ResetProcess(this._id);
            if (ret != 0) { throw new Exception(GetError(ret)); }

            this.CH_Num = ch_Num;


            this._IsInit = true;
        }

        /// <summary>
        /// デバイスの解放を行います
        /// </summary>
        public void Exit()
        {
            if (this._IsInit == true)
            {
                var ret = this._caio.Exit(this._id);
                if (ret != 0) { throw new Exception(GetError(ret)); }

                this._IsInit = false;
            }
        }

        /// <summary>
        /// 指定したデバイスの強制解放を行います
        /// </summary>
        public static void Exit(short id)
        {
            Caio caio = new Caio();
            try
            {
                var ret = caio.Exit(id);
                if (ret != 0)
                {
                    var errormes = "";
                    var ret2 = caio.GetErrorString(ret, out errormes);
                    throw new Exception($"強制終了に失敗しました。\n{errormes}");
                }
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        /// <summary>
        /// ロギングを開始します
        /// </summary>
        /// <param name="token"></param>
        public void Start()
        {
            this._Data.Clear();
            this._result = ResultInfo.NoLogging;
            this._state = LoggingState.StartOn;
            this._stopFlg = false;
            try
            {
                if (this._IsInit != true)
                {
                    Init(this._ch_Num);
                }

                // メモリーリセット
                var ret = this._caio.ResetAiMemory(this._id);
                if (ret != 0) { throw new Exception(GetError(ret)); }

                ret = this._caio.ResetDevice(this._id);
                if (ret != 0) { throw new Exception(GetError(ret)); }

                ret = this._caio.SetAiStopTimes(this._id, this._MaxLoggingCount);
                if (ret != 0) { throw new Exception(GetError(ret)); }
                //this._caio.SetAiEventSamplingTimes(this._id, this._MaxLoggingCount).ConsoleShow();

                // ロギングスタート
                ret = this._caio.StartAi(this._id);
                if (ret != 0) { throw new Exception(GetError(ret)); }

                // ロギングデータ格納バッファ作成
                for (int i = 0; i < this._ch_Num; i++)
                {
                    this._Data.Add(new List<float>());
                }

                // 終了フラグがTrueになるか、最大データ量をオーバーするまでサンプリング

                long totalSamplingCount = 0;
                this._state = LoggingState.Logging;
                DateTime start = DateTime.Now;
                while (!this._stopFlg)
                {
                    int count;
                    ret = this._caio.GetAiSamplingCount(this._id, out count);   // 現在のサンプリング数を取得
                    if (ret != 0) { throw new Exception(GetError(ret)); }

                    int status;
                    ret = this._caio.GetAiStatus(this._id, out status);
                    if (ret != 0) { throw new Exception(GetError(ret)); }

                    // 最大ロギング上限を超えていたら終了する
                    if (totalSamplingCount > this._MaxLoggingCount || count > this._MaxLoggingCount)
                    {
                        this._result = ResultInfo.LogLimitOver;
                        break;
                    }

                    // バッファカウント数が許容量を超えていたら一度データを吸い上げる
                    if (count > this._safeCount)
                    {
                        totalSamplingCount += count;
                        this.DataSuction(this._Data, count);
                    }

                    // TimeOut
                    if ((DateTime.Now - start).TotalSeconds > this._timeOut)
                    {
                        this._result = ResultInfo.LogLimitOver;
                        break;
                    }
                }

                // サンプリング終了処理
                ret = this._caio.StopAi(this._id);
                if (ret != 0) { throw new Exception(GetError(ret)); }

                this._state = LoggingState.DataMaiking;


                // バッファ内のデータをすべて吸い上げる
                // メモリ内格納サンプリング数の取得
                int datacount;
                ret = this._caio.GetAiSamplingCount(this._id, out datacount);
                if (ret != 0) { throw new Exception(GetError(ret)); }

                // データの吸い上げ
                this.DataSuction(this._Data, datacount);

                this._state = LoggingState.Standby;
                if (this._result != ResultInfo.LogLimitOver)
                {
                    this._result = ResultInfo.OK;
                }

            }
            catch (Exception err)
            {
                this._result = ResultInfo.Fatal;
                this._state = LoggingState.Error;
                this._caio.Exit(this._id);
                this.Init(this._ch_Num);
                throw;
            }
        }

        /// <summary>
        /// 非同期でロギングを開始します
        /// </summary>
        /// <param name="token"></param>
        public async Task<List<List<float>>> StartAsync()
        {
            List<List<float>> dataList = new List<List<float>>();
            this._result = ResultInfo.NoLogging;
            this._state = LoggingState.StartOn;
            this._stopFlg = false;

            Task<List<List<float>>> task = Task.Run(() =>
            {
                try
                {               

                    if (_IsInit != true)
                    {
                        Init(this._ch_Num);
                    }

                    // プロセスリセット
                    var ret = this._caio.ResetProcess(this._id);
                    if (ret != 0) { throw new Exception(GetError(ret)); }

                    // device Reset
                    ret = this._caio.ResetDevice(this._id);
                    if (ret != 0) { throw new Exception(GetError(ret)); }

                    // メモリーリセット
                    ret = this._caio.ResetAiMemory(this._id);
                    if (ret != 0) { throw new Exception(GetError(ret)); }
                                       

                    this.CH_Num     = this._ch_Num;
                    this.ScanRate   = this._scanRate;


                    ret = this._caio.SetAiStopTimes(this._id, this._MaxLoggingCount);
                    if (ret != 0) { throw new Exception(GetError(ret)); }


                    // ロギングスタート
                    ret = this._caio.StartAi(this._id);
                    if (ret != 0) { throw new Exception(GetError(ret)); }
                    // ロギングデータ格納バッファ作成
                    for (int i = 0; i < this._ch_Num; i++)
                    {
                        dataList.Add(new List<float>());
                    }

                    // 終了フラグがTrueになるか、最大データ量をオーバーするまでサンプリング

                    long totalSamplingCount = 0;
                    this._state = LoggingState.Logging;
                    DateTime start = DateTime.Now;
                    while (!this._stopFlg)
                    {
                        int count;
                        ret = this._caio.GetAiSamplingCount(this._id, out count);   // 現在のサンプリング数を取得
                        if (ret != 0) { throw new Exception(GetError(ret)); }

                        int status;
                        ret = this._caio.GetAiStatus(this._id, out status);
                        if (ret != 0) { throw new Exception(GetError(ret)); }

                        // 最大ロギング上限を超えていたら終了する
                        if (totalSamplingCount > this._MaxLoggingCount || count > this._MaxLoggingCount)
                        {
                            this._result = ResultInfo.LogLimitOver;
                            break;
                        }

                        // バッファカウント数が許容量を超えていたら一度データを吸い上げる
                        if (count > this._safeCount)
                        {
                            totalSamplingCount += count;
                            this.DataSuction(dataList, count);
                        }

                        // TimeOut
                        if ((DateTime.Now - start).TotalSeconds > this._timeOut)
                        {
                            this._result = ResultInfo.LogLimitOver;
                            break;
                        }
                    }

                    // サンプリング終了処理
                    ret = this._caio.StopAi(this._id);
                    if (ret != 0) { throw new Exception(GetError(ret)); }

                    this._state = LoggingState.DataMaiking;


                    // バッファ内のデータをすべて吸い上げる
                    // メモリ内格納サンプリング数の取得
                    int datacount;
                    ret = this._caio.GetAiSamplingCount(this._id, out datacount);
                    if (ret != 0) { throw new Exception(GetError(ret)); }

                    // データの吸い上げ
                    this.DataSuction(dataList, datacount);

                    this._state = LoggingState.Standby;
                    if (this._result != ResultInfo.LogLimitOver)
                    {
                        this._result = ResultInfo.OK;
                    }

                    return dataList;

                }
                catch (Exception err)
                {
                    this._result = ResultInfo.Fatal;
                    this._state = LoggingState.Error;
                    this._caio.Exit(this._id);
                    this.Init(this._ch_Num);
                    throw;
                }
            });

            return await task;
        }
        #region PrivateMethods

        /// <summary>
        /// エラーコードからエラーメッセージを取得します
        /// </summary>
        /// <param name="ret">エラーコード</param>
        /// <returns>エラーメッセージ</returns>
        private string GetError(int ret)
        {
            string err = "";
            var ret2 = this._caio.GetErrorString(ret, out err);
            if (ret2 != 0)
            {
                return String.Format("ErrorCode: {0}\n{1}", ret, ret2);
            }
            else
            {
                return String.Format("ErrorCode: {0}\n{1}", ret, err);
            }
        }

        /// <summary>
        /// Contecからデータを吸いあげる
        /// </summary>
        /// <param name="dataList"></param>
        private void DataSuction(List<List<float>> dataList, int _count)
        {
            int dataCount = _count;
            //int dataCount = this._ch_Num * _count;
            var ch = this.CH_Num;
            float[] datas = new float[dataCount * ch];

            var ret = this._caio.GetAiSamplingDataEx(this._id, ref dataCount, ref datas);
            if (ret != 0) { throw new Exception(GetError(ret)); }

            /*
             datas に格納されているデータは、[ch1,ch2,ch3,ch1,ch2,ch3,.....]のように並んでいる
             */

            for (int idx = 0; idx < (datas.Length / ch); idx++)
            {
                for (int chN = 0; chN < this._ch_Num; chN++)
                {
                    dataList[chN].Add(datas[(idx * ch) + chN]);
                }
            }

        }


        #endregion

        #region ToDo

        // SetAiStopTimes

        #endregion
    }
}
