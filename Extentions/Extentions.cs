﻿using System;
using System.Text;
using Newtonsoft.Json;

namespace Utility
{
    public static class Const
    {
        const string Json_Linq_JObject = "Newtonsoft.Json.Linq.JObject";
    }

    /// <summary>
    /// 例外発生時、例外リリースの代わりに実行されるコールバック関数
    /// </summary>
    /// <param name="err">発生した例外</param>
    public delegate void NotifyExceptionCallBack(Exception err);
    
    public static class Extentions
    {         
        /// <summary>
        /// コンソールへ内容を表示します
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="val"></param>
        public static void ConsoleShow<T>(this T val)
        {
            System.Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(val);
        }

        /// <summary>
        /// コンソールへ内容を表示し、キー入力を待ちます
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="val"></param>
        public static void ConsoleShow_Wait<T>(this T val)
        {
            System.Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(val);
            Console.ReadKey();

            
        }
         
        /// <summary>
        /// ファイルへ保存します
        /// </summary>
        /// <param name="val"></param>
        /// <param name="fileName">保存するファイル名（フルパス）</param>
        /// <param name="append">true: 末尾へ追加 false: 上書き</param>
        public static void FileDump(this string val, string fileName, bool append = false)
        {

            val.FileDump(fileName, Encoding.UTF8, append);
        }

        /// <summary>
        /// ファイルへ保存します
        /// </summary>
        /// <param name="val"></param>
        /// <param name="fileName">保存するファイル名（フルパス）</param>
        /// <param name="encoding"></param>
        /// <param name="append">true: 末尾へ追加 false: 上書き</param>
        public static void FileDump(this string val, string fileName, Encoding encoding, bool append = false)
        {
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fileName, append, encoding))
            {
                sw.Write(val);
            }
        }

        /// <summary>
        /// ファイルへ保存します
        /// </summary>
        /// <param name="val"></param>
        /// <param name="fileName">保存するファイル名（フルパス）</param>
        /// <param name="encoding"></param>
        /// <param name="func">例外発生時のコールバック関数</param>
        /// <param name="append">true: 末尾へ追加 false: 上書き</param>
        public static void FileDump(this string val, string fileName, Encoding encoding, NotifyExceptionCallBack func, bool append = false)
        {
            try
            {
                val.FileDump(fileName, encoding, append);                
            }
            catch (Exception err)
            {
                func(err);
            }
            
        }        

        /// <summary>
        /// ObjectをJson文字列へ変換します
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="formatting">インデントするかの指定</param>
        /// <returns></returns>
        public static string JsonDumpStr<T>(this T obj, Formatting formatting = Formatting.Indented)
        {
            string jsonString = JsonConvert.SerializeObject(obj, formatting);

            return jsonString;
        }

        /// <summary>
        /// ObjectをJson文字列へ変換します
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="func">例外発生時のコールバック関数</param>
        /// <param name="formatting">インデントするかの指定</param>
        /// <returns></returns>
        public static string JsonDumpStr<T>(this T obj, NotifyExceptionCallBack func, Formatting formatting = Formatting.Indented)
        {
            try
            {
                string jsonString = obj.JsonDumpStr(formatting);

                return jsonString;
            }
            catch (Exception err)
            {
                func(err);
                return null;
            }
            
        }

        /// <summary>
        /// JsonStringをオブジェクトへ変換する
        /// </summary>
        /// <typeparam name="T">オブジェクトの型</typeparam>
        /// <param name="jsonString"></param>
        /// <returns>変換後のオブジェクト</returns>
        public static T JsonStrToObj<T>(this string jsonString)
        {
            T obj = JsonConvert.DeserializeObject<T>(jsonString);

            return obj;
        }

        /// <summary>
        /// JsonStringをオブジェクトへ変換する
        /// </summary>
        /// <typeparam name="T">オブジェクトの型</typeparam>
        /// <param name="jsonString"></param>
        /// <param name="func">例外発生時のコールバック関数</param>
        /// <returns>変換後のオブジェクト</returns>
        public static T JsonStrToObj<T>(this string jsonString, NotifyExceptionCallBack func)
        {
            try
            {
                T obj = JsonConvert.DeserializeObject<T>(jsonString);
                return obj;
            }
            catch (Exception err)
            {
                func(err);
                return default(T);
            }
        }

        /// <summary>
        /// 16進数文字列をint型10進数へ変換する
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int HexToInt(this string str)
        {
            int num16 = Convert.ToInt32(str, 16);
            return num16;
        }

        /// <summary>
        /// 16進数文字列をint型10進数へ変換する
        /// </summary>
        /// <param name="str"></param>
        /// <param name="func">例外発生時のコールバック関数</param>
        /// <returns></returns>
        public static int HexToInt(this string str, NotifyExceptionCallBack func)
        {
            try
            {
                int num16 = Convert.ToInt32(str, 16);
                return num16;
            }
            catch (Exception err)
            {
                func(err);
                return -1;
            }
            
        }

        /// <summary>
        /// 10進数を16進数の文字列へ変換する
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string HexToString(this int val)
        {
            string hexStr = Convert.ToString(val, 16);
            return hexStr;
        }
    }

    public static class Util
    {
        /// <summary>
        /// ファイルを読み込みます
        /// </summary>
        /// <param name="fileName">読み込むファイル名(フルパス)</param>
        /// <param name="encoding">ファイルエンコーディング</param>
        /// <param name="func">例外発生時のコ－ルバック</param>
        /// <returns></returns>
        public static string ReadFile(string fileName, Encoding encoding, NotifyExceptionCallBack func)
        {
            string fileString;

            try
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(fileName, encoding))
                {
                    fileString = sr.ReadToEnd();
                }

                return fileString;
            }
            catch (Exception err)
            {
                func(err);
                return null;
            }
            
        }

        /// <summary>
        /// ファイルを読み込みます
        /// </summary>
        /// <param name="fileName">読み込むファイル名(フルパス)</param>
        /// <param name="encoding">ファイルエンコーディング</param>
        /// <returns></returns>
        public static string ReadFile(string fileName, Encoding encoding)
        {
            string fileString;
            using (System.IO.StreamReader sr = new System.IO.StreamReader(fileName, encoding))
            {
                fileString = sr.ReadToEnd();
            }

            return fileString;
        }

        /// <summary>
        /// ファイルを読み込みます
        /// </summary>
        /// <param name="fileName">読み込むファイル名(フルパス)</param>
        /// <returns></returns>
        public static string ReadFile(string fileName)
        {
            string fileString;
            using (System.IO.StreamReader sr = new System.IO.StreamReader(fileName, Encoding.UTF8))
            {
                fileString = sr.ReadToEnd();
            }

            return fileString;
        }
        
    }
}
