﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using System.Linq.Expressions;


namespace Utility.Tensor
{
    public static class Cartesian
    {
        /// <summary>
        /// 円柱座標 → カルテシアン座標 変換
        /// </summary>
        /// <param name="r">半径r</param>
        /// <param name="th">角度θ[deg]</param>
        /// <param name="z">高さz</param>
        /// <returns></returns>
        public static Vector<double> Cyl2xyz(double r, double th, double z)
        {
            var xyz = Vector<Double>.Build.Dense(3);
            xyz[0] = r * Math.Cos(th * Math.PI / 180.0);
            xyz[1] = r * Math.Sin(th * Math.PI / 180.0);
            xyz[2] = z;
            return (xyz);
        }

        /// <summary>
        /// 3点を通る平面の法線ベクトルと符号付き距離を計算する
        /// </summary>
        /// <param name="va"></param>点a
        /// <param name="vb"></param>点b
        /// <param name="vc"></param>点c
        /// <param name="vnn">output 法線ベクトル</param>
        /// <param name="s">output 原点からの符号付き距離</param>
        public static void Plane(Vector<double> va, Vector<double> vb, Vector<double> vc, out Vector<double> vnn, out double s)
        {
            var ab = vb - va;
            var bc = vc - vb;
            var ca = vc - va;
            if (ab.L2Norm() * bc.L2Norm() * ca.L2Norm() < 0.001)    //いずれか2点が一致する場合は平面が一意に定まらない
            {
                throw new ArgumentException("3点のうち2点以上が近すぎるため、平面が一意に定まりません");
            }
            Vector<double> vn = CrossPuroduct3D(va, vb) + CrossPuroduct3D(vb, vc) + CrossPuroduct3D(vc, va);
            double norm = vn.L2Norm();
            vnn = vn.Normalize(2);
            s = vc.DotProduct(CrossPuroduct3D(va, vb)) / norm;
        }

        /// <summary>
        /// 平面の符号付距離を計算する
        /// </summary>
        /// <param name="centerVec">中心ベクトル</param>
        /// <param name="planeVec">法線ベクトル</param>
        /// <returns></returns>
        public static double SignedDistance(Vector<double> centerVec, Vector<double> planeVec)
        {
            double d = centerVec * planeVec;
            double SD = d / Math.Sqrt(Math.Pow(planeVec[0],2) + Math.Pow(planeVec[1], 2) + Math.Pow(planeVec[2], 2));
            return SD;
        }


        /// <summary>
        /// 円周上の3点から中心座標と半径を計算する
        /// </summary>
        /// <param name="va"></param>円周上の点aの座標
        /// <param name="vb"></param>円周上の点bの座標
        /// <param name="vc"></param>円周上の点cの座標
        /// <param name="center">output 中心座標</param>
        /// <param name="r">output 半径</param>
        public static void CircumCenter(Vector<double> va, Vector<double> vb, Vector<double> vc, out Vector<double> center, out double r)
        {
            var ab = vb - va;
            var bc = vc - vb;
            var ca = vc - va;
            if (Math.Abs(CrossPuroduct3D(ab, ca).L2Norm()) < 0.001) //3点が直線状にある場合は円が決定できない
            {
                throw new ArgumentException("3点が直線状にあるため、円が決定できません");
            }
            if (ab.L2Norm() * bc.L2Norm() * ca.L2Norm() < 0.001)    //いずれか2点が一致する場合は円が一意に定まらない
            {
                throw new ArgumentException("3点のうち2点が近すぎるため、円が一意に定まりません");
            }
            var aa = Math.Pow((vb - vc).L2Norm(), 2.0);
            var bb = Math.Pow((vc - va).L2Norm(), 2.0);
            var cc = Math.Pow((va - vb).L2Norm(), 2.0);

            double[] origin = { 0, 0, 0 };
            center = Vector<double>.Build.Dense(origin);
            center = center.Add(va * (aa * (bb + cc - aa)));
            center = center.Add(vb * (bb * (cc + aa - bb)));
            center = center.Add(vc * (cc * (aa + bb - cc)));
            center = center.Divide(2 * (aa * bb + bb * cc + cc * aa) - (Math.Pow(aa, 2.0) + Math.Pow(bb, 2.0) + Math.Pow(cc, 2.0)));
            r = (va - center).L2Norm();
        }

        /// <summary>
        /// 平面と直線の交点の座標を計算する
        /// </summary>
        /// <param name="v_plane">平面の法線ベクトル</param>
        /// <param name="h_plane">平面の符号付き距離</param>
        /// <param name="p_line">直線上の点の位置ベクトル</param>
        /// <param name="v_line">直線の方向ベクトル</param>
        /// <returns></returns>
        public static Vector<double> IntersectionPlaneLine(Vector<double> v_plane, double h_plane, Vector<double> p_line, Vector<double> v_line)
        {
            if (CrossPuroduct3D(v_plane, p_line).L2Norm() < 0.001) //直線が平面上にある場合は交点が計算できない
            {
                throw new ArgumentException("直線が平面上にあるため、交点が計算できません");
            }
            double val = (h_plane - v_plane.DotProduct(p_line)) / v_plane.DotProduct(v_line);
            Vector<double> v_intersection = p_line + v_line * val;
            return (v_intersection);
        }

        /// <summary>
        /// 移動方向に沿って目標点に近づいた位置を計算する
        /// </summary>
        /// <param name="len"></param>移動量
        /// <param name="ini_p"></param>初期位置
        /// <param name="vec"></param>移動方向
        /// <param name="dir_p"></param>目標位置
        /// <returns></returns>
        public static Vector<double> Length_n_Point(double len, Vector<double> ini_p, Vector<double> vec, Vector<double> dir_p)
        {
            double h_del = vec.DotProduct(ini_p - dir_p);
            if (Math.Abs(h_del) < 0.001)
            {
                throw new ArgumentException("目標が移動方向に対して垂直にあるため、移動できません");
            }
            else if (h_del > 0)
            {
                return (ini_p - vec * (len / vec.L2Norm()));
            }
            else
            {
                return (ini_p + vec * (len / vec.L2Norm()));
            }
        }

        /// <summary>
        /// 2ベクトルの回転角度[deg]と回転軸ベクトルを計算する
        /// </summary>
        /// <param name="vi">始点ベクトル</param>
        /// <param name="vf">終点べクトル</param>
        /// <param name="angle">output 回転角度[deg]</param>
        /// <param name="vn">output 回転軸ベクトル[deg]</param>
        public static void Angle(Vector<double> vi, Vector<double> vf, out double angle, out Vector<double> vn)
        {

            vi = vi.Divide(vi.L2Norm());
            vf = vf.Divide(vf.L2Norm());
            double dot = vi.DotProduct(vf);
            var cro = CrossPuroduct3D(vi, vf);
            angle = Math.Atan2(cro.L2Norm(), dot) * 180 / Math.PI;
            if (CrossPuroduct3D(vi, vf).L2Norm() < 0.0001) //2ベクトルが平行な時は回転軸ベクトルを計算できない
            {
                vn = Vector<double>.Build.Dense(3);
            }
            else
            {
                vn = cro / cro.L2Norm();
            }
        }

        /// <summary>
        /// ロールピッチヨー → 回転行列 変換
        /// </summary>
        /// <param name="r">ロール角[deg]</param>
        /// <param name="p">ピッチ角[deg]</param>
        /// <param name="y">ヨー角[deg]</param>
        /// <returns></returns>
        public static Matrix<double> RPY2Rmat(double r, double p, double y)
        {
            double sa = Math.Sin(r * Math.PI / 180.0);
            double ca = Math.Cos(r * Math.PI / 180.0);
            double sb = Math.Sin(p * Math.PI / 180.0);
            double cb = Math.Cos(p * Math.PI / 180.0);
            double sc = Math.Sin(y * Math.PI / 180.0);
            double cc = Math.Cos(y * Math.PI / 180.0);

            double[,] rmat = new double[3, 3]{ { ca*cb           , sa*cb           , -sb   },
                                               {-sa*cc + sb*sc*ca, sa*sb*sc + ca*cc, sc*cb },
                                               { sa*sc + sb*ca*cc, sa*sb*cc - sc*ca, cb*cc } };

            var rotateMat = Matrix<double>.Build.DenseOfArray(rmat);
            return (rotateMat);
        }

        /// <summary>
        /// オイラー角 → 回転行列 変換
        /// </summary>
        /// <param name="a">?軸回りの回転角[deg]</param>
        /// <param name="b">?軸回りの回転角[deg]</param>
        /// <param name="c">?軸回りの回転角[deg]</param>
        /// <returns></returns>
        public static Matrix<double> Euler2Rmat(double a, double b, double c)
        {
            double sa = Math.Sin(a * Math.PI / 180.0);
            double ca = Math.Cos(a * Math.PI / 180.0);
            double sb = Math.Sin(b * Math.PI / 180.0);
            double cb = Math.Cos(b * Math.PI / 180.0);
            double sc = Math.Sin(c * Math.PI / 180.0);
            double cc = Math.Cos(c * Math.PI / 180.0);

            double[,] rmat = new double[3, 3]{ {-sa*sc + ca*cb*cc,  sa*cb*cc + sc*ca, -sb*cc },
                                               {-sa*cc - sc*ca*cb, -sa*sc*cb + ca*cc,  sb*sc },
                                               { sb*ca           ,  sa*sb           ,  cb    } };

            var rotateMat = Matrix<double>.Build.DenseOfArray(rmat);
            return (rotateMat);
        }

        /// <summary>
        /// 回転行列 → ロールピッチヨー 変換
        /// </summary>
        /// <param name="m">回転行列</param>
        /// <param name="r">output ロール角[deg]</param>
        /// <param name="p">output ピッチ角[deg]</param>
        /// <param name="y">otuput ヨー角[deg]</param>
        public static void Rmat2RPY(Matrix<double> m, out double r, out double p, out double y)
        {
            double cp = Math.Sqrt(Math.Pow(m[0, 0], 2) + Math.Pow(m[0, 1], 2));
            double sp = -m[0, 2];

            if (cp < 0.00001)    //縮退状態
            {
                r = 0.0;        //基準値を0に固定
                if (sp < 0.0)
                {
                    p = -Math.PI / 2.0;
                    y = Math.Atan2(-m[1, 0], m[1, 1]);
                }
                else
                {
                    p = Math.PI / 2.0;
                    y = Math.Atan2(m[1, 0], m[1, 1]);
                }
            }
            else
            {
                p = Math.Atan2(sp, cp);
                cp = Math.Cos(p);
                r = Math.Atan2(m[0, 1] / cp, m[0, 0] / cp);
                y = Math.Atan2(m[1, 2] / cp, m[2, 2] / cp);
            }
            r *= 180.0 / Math.PI;
            p *= 180.0 / Math.PI;
            y *= 180.0 / Math.PI;
        }

        /// <summary>
        /// 回転行列 → オイラー角 変換
        /// </summary>
        /// <param name="m">回転行列</param>
        /// <param name="a">output ?軸回りの回転角[deg]</param>
        /// <param name="b">output ?軸回りの回転角[deg]</param>
        /// <param name="c">output ?軸回りの回転角[deg]</param>
        public static void Rmat2Euler(Matrix<double> m, out double a, out double b, out double c)
        {
            double sb = Math.Sqrt(Math.Pow(m[0, 2], 2) + Math.Pow(m[1, 2], 2));
            double cb = m[2, 2];
            if (sb < 0.00001)    //特異解が存在
            {
                a = 0.0;        //基準角を0
                if (cb < 0.0)
                {
                    b = Math.PI;
                    c = Math.Atan2(m[1, 0], -m[0, 0]);
                }
                else
                {
                    b = 0.0;
                    c = Math.Atan2(-m[1, 0], m[0, 0]);
                }
            }
            else
            {
                b = Math.Atan2(sb, cb);
                sb = Math.Sin(b);
                a = Math.Atan2(m[2, 1] / sb, m[2, 0] / sb);
                c = Math.Atan2(m[1, 2] / sb, -m[0, 2] / sb);
            }
            a *= 180.0 / Math.PI;
            b *= 180.0 / Math.PI;
            c *= 180.0 / Math.PI;
        }

        /// <summary>
        /// ロールピッチヨー → オイラー角 変換
        /// 回転行列を介して変換
        /// </summary>
        /// <param name="r">ロール角[deg]</param>
        /// <param name="p">ピッチ角[deg]</param>
        /// <param name="y">ヨー角[deg]</param>
        /// <param name="a">output ?軸回りの回転角[deg]</param>
        /// <param name="b">output ?軸回りの回転角[deg]</param>
        /// <param name="c">output ?軸回りの回転角[deg]</param>
        public static void RPY2Euler(double r, double p, double y, out double a, out double b, out double c)
        {
            var rmat = RPY2Rmat(r, p, y);
            Rmat2Euler(rmat, out a, out b, out c);
        }

        /// <summary>
        /// オイラー角 → ロールピッチヨー 変換
        /// </summary>
        /// <param name="a">?軸回りの回転角[deg]</param>
        /// <param name="b">?軸回りの回転角[deg]</param>
        /// <param name="c">?軸回りの回転角[deg]</param>
        /// <param name="r">output ロール角[deg]</param>
        /// <param name="p">output ピッチ角[deg]</param>
        /// <param name="y">otuput ヨー角[deg]</param>
        public static void Euler2RPY(double a, double b, double c, out double r, out double p, out double y)
        {
            var rmat = Euler2Rmat(a, b, c);
            Rmat2RPY(rmat, out r, out p, out y);
        }

        /// <summary>
        /// 回転軸と回転角度から回転行列を計算する
        /// </summary>
        /// <param name="r">回転角度[deg]</param>
        /// <param name="nv">回転軸ベクトル</param>
        /// <returns></returns>
        public static Matrix<double> Vec_route2Rmat(Vector<double> nv, double r)
        {
            nv = nv.Normalize(2);
            double nx = nv[0];
            double ny = nv[1];
            double nz = nv[2];
            double ct = Math.Cos(r * Math.PI / 180.0);
            double st = Math.Sin(r * Math.PI / 180.0);
            double ct2 = (1 - ct);

            double[,] rmat = new double[3, 3]{ {nx*nx*ct2 +    ct, nx*ny*ct2 - nz*st, nz*nx*ct2 + ny*st},
                                               {nx*ny*ct2 + nz*st, ny*ny*ct2 +    ct, ny*nz*ct2 - nx*st},
                                               {nx*nz*ct2 - ny*st, nz*ny*ct2 + nx*st, nz*nz*ct2 +    ct} };

            var rotateMat = Matrix<double>.Build.DenseOfArray(rmat);
            return (rotateMat);
        }

        /// <summary>
        /// 3次元ベクトルの外積α×βを計算する
        /// </summary>
        /// <param name="va">第1ベクトルα</param>
        /// <param name="vb">第2ベクトルβ</param>
        /// <returns></returns>
        public static Vector<double> CrossPuroduct3D(Vector<double> va, Vector<double> vb)
        {
            if (va.Count != 3 || vb.Count != 3)
            {
                throw new ArgumentException("入力されたベクトルが3次元ではありません");
            }
            var vc = Vector<double>.Build.Dense(3);
            vc[0] = va[1] * vb[2] - va[2] * vb[1];
            vc[1] = va[2] * vb[0] - va[0] * vb[2];
            vc[2] = va[0] * vb[1] - va[1] * vb[0];

            return (vc);
        }
        
    }
    
    /// <summary>
    /// 3要素ベクトルのインターフェイス
    /// </summary>
    public interface IVec3D
    {
        double Dot(IVec3D vec3);
        IVec3D Cross(IVec3D vec3);
        Vector<double> Vector { get; }
        float[] ToArrary_F();
        double[] ToArrary();
        double L1Norm
        {
            get;
        }
        double L2Norm
        {
            get;
        }
    }

    /// <summary>
    /// 6要素ベクトルのインターフェイス
    /// </summary>
    public interface IVec6D
    {
        double Dot(IVec6D vec3);
        IVec6D Cross(IVec6D vec3);
        Vector<double> Vector { get; }
        float[] ToArrary_F();
        double[] ToArrary();

        double L1Norm
        {
            get;
        }

        double L2Norm
        {
            get;
        }
    }

    /// <summary>
    /// 3×3 要素行列のインターフェイス
    /// </summary>
    public class Mat3D_W
    {
        protected Matrix<double> _matrix;

        public Mat3D_W()
        {
            this._matrix = Matrix<double>.Build.Dense(3, 3, 0);
        }

        public Mat3D_W(Vec3D_base row1, Vec3D_base row2, Vec3D_base row3)
        {
            this._matrix = Matrix<double>.Build.DenseOfColumnVectors(new Vector<double>[3] { row1.Vector, row2.Vector, row3.Vector });
            //this._matrix = Matrix<double>.Build.DenseOfColumnVectors(new Vector<double>[3]);
            this._matrix[0, 0] = row1[0];
            this._matrix[0, 1] = row2[0];
            this._matrix[0, 2] = row3[0];

            this._matrix[1, 0] = row1[1];
            this._matrix[1, 1] = row2[1];
            this._matrix[1, 2] = row3[1];

            this._matrix[2, 0] = row1[2];
            this._matrix[2, 1] = row2[2];
            this._matrix[2, 2] = row3[2];
        }

        public Matrix<double> Matrix
        {
            get => this._matrix;
            set => this._matrix = value;
        }

        public Mat3D_W Inverse
        {
            get
            {
                var revMat = new Mat3D_W();
                revMat._matrix = this._matrix.Inverse();
                return revMat;
            }
        }

        /// <summary>
        /// this * mat3(内積)を返します
        /// </summary>
        /// <param name="mat3"></param>
        /// <returns></returns>
        public Mat3D_W Dot(Mat3D_W mat3)
        {
            return new Mat3D_W() { _matrix = this._matrix * mat3._matrix };
        }

        public double this[int row, int col]
        {
            get => this._matrix[row, col];
            set => this._matrix[row, col] = value;
        }

        public RPY RPY
        {
            get
            {
                Cartesian.Rmat2RPY(this._matrix, out double r, out double p, out double y);
                return new RPY(r, p, y);
            }
            set
            {
                this._matrix = Cartesian.RPY2Rmat(value.Role, value.Pitch, value.Yaw);
            }
        }

        public Vec3D_base Euler
        {
            get
            {
                Cartesian.Rmat2Euler(this._matrix, out double a, out double b, out double c);
                return new RPY(a, b, c);
            }
            set
            {
                this._matrix = Cartesian.RPY2Rmat(value[0], value[1], value[2]);
            }
        }

        #region operator
        public static Mat3D_W operator +(Mat3D_W left, Mat3D_W right)
        {
            return new Mat3D_W() { _matrix = left._matrix + right._matrix };
        }
        public static Mat3D_W operator +(Mat3D_W left, double right)
        {
            return new Mat3D_W() { _matrix = left._matrix + right };
        }
        public static Mat3D_W operator +(double left, Mat3D_W right)
        {
            return new Mat3D_W() { _matrix = left + right._matrix };
        }
        public static Mat3D_W operator -(Mat3D_W left, Mat3D_W right)
        {
            return new Mat3D_W() { _matrix = left._matrix - right._matrix };
        }
        public static Mat3D_W operator -(Mat3D_W left, double right)
        {
            return new Mat3D_W() { _matrix = left._matrix - right };
        }
        public static Mat3D_W operator -(double left, Mat3D_W right)
        {
            return new Mat3D_W() { _matrix = left - right._matrix };
        }
        public static Mat3D_W operator *(Mat3D_W left, Mat3D_W right)
        {
            return new Mat3D_W() { _matrix = left._matrix * right._matrix };
        }
        public static Mat3D_W operator *(Mat3D_W left, double right)
        {
            return new Mat3D_W() { _matrix = left._matrix * right };
        }
        public static Mat3D_W operator *(double left, Mat3D_W right)
        {
            return new Mat3D_W() { _matrix = left * right._matrix };
        }        
        public static Mat3D_W operator /(Mat3D_W left, double right)
        {
            return new Mat3D_W() { _matrix = left._matrix / right };
        }
        public static Mat3D_W operator /(double left, Mat3D_W right)
        {
            return new Mat3D_W() { _matrix = left / right._matrix };
        }
        #endregion
    }

    /// <summary>
    /// 3要素ベクトル基底クラス
    /// </summary>
    public class Vec3D_base : IVec3D
    {
        protected Vector<double> _vector;
        public Vector<double> Vector
        {
            get => this._vector;
        }

        public Vec3D_base()
        {
            this._vector = Vector<double>.Build.Dense(3, default(double));
        }

        public Vec3D_base(double idx1, double idx2, double idx3)
        {
            this._vector = Vector<double>.Build.Dense(new double[3] { idx1, idx2, idx3 });
        }

        public IVec3D Cross(IVec3D vec3)
        {
            var vc = new Vec3D_base();
            vc[0] = this.Vector[1] * vec3.Vector[2] - this.Vector[2] * vec3.Vector[1];
            vc[1] = this.Vector[2] * vec3.Vector[0] - this.Vector[0] * vec3.Vector[2];
            vc[2] = this.Vector[0] * vec3.Vector[1] - this.Vector[1] * vec3.Vector[0];
            return vc;
        }

        public double L1Norm
        {
            get => this._vector.L1Norm();
        }

        public double L2Norm
        {
            get => this._vector.L2Norm();
        }

        public double Dot(IVec3D vec3)
        {
            return this._vector.DotProduct(vec3.Vector);
        }

        public double this[int idx]
        {
            get => this._vector[idx];
            set => this._vector[idx] = value;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public float[] ToArrary_F()
        {
            return new float[] { Convert.ToSingle(this._vector[0]), Convert.ToSingle(this._vector[1]), Convert.ToSingle(this._vector[2]) };
        }

        public double[] ToArrary()
        {
            return new double[] { this._vector[0], this._vector[1], this._vector[2] };
        }

        #region operator
        public static Vec3D_base operator +(Vec3D_base left, Vec3D_base right)
        {
            return new Vec3D_base() { _vector = left._vector + right._vector };
        }
        public static Vec3D_base operator +(Vec3D_base left, double right)
        {
            return new Vec3D_base(){_vector = left._vector + right};
        }
        public static Vec3D_base operator +(double left, Vec3D_base right)
        {
            return new Vec3D_base() { _vector = left + right._vector };
        }
        public static Vec3D_base operator -(Vec3D_base left, Vec3D_base right)
        {
            return new Vec3D_base() { _vector = left._vector - right._vector };
        }
        public static Vec3D_base operator -(Vec3D_base left, double right)
        {
            return new Vec3D_base() { _vector = left._vector - right };
        }
        public static Vec3D_base operator -(double left, Vec3D_base right)
        {
            return new Vec3D_base() { _vector = left - right._vector };
        }
        public static double operator *(Vec3D_base left, Vec3D_base right)
        {
            return left._vector * right._vector;
        }
        public static Vec3D_base operator *(Vec3D_base left, double right)
        {
            return new Vec3D_base() { _vector = left._vector * right };
        }
        public static Vec3D_base operator *(double left, Vec3D_base right)
        {
            return new Vec3D_base() { _vector = left * right._vector };
        }
        public static Vec3D_base operator /(Vec3D_base left, Vec3D_base right)
        {
            return new Vec3D_base() { _vector = left._vector / right._vector };
        }
        public static Vec3D_base operator /(Vec3D_base left, double right)
        {
            return new Vec3D_base() { _vector = left._vector / right };
        }
        public static Vec3D_base operator /(double left, Vec3D_base right)
        {
            return new Vec3D_base() { _vector = left / right._vector };
        }
        #endregion
    }

    /// <summary>
    /// 6要素ベクトル基底クラス
    /// </summary>
    public class Vec6D_base : IVec6D
    {
        protected Vector<double> _vector;
        public Vector<double> Vector
        {
            get => this._vector;
        }

        public Vec6D_base()
        {
            this._vector = Vector<double>.Build.Dense(6, default(double));
        }

        public Vec6D_base(double idx1, double idx2, double idx3, double idx4, double idx5, double idx6)
        {
            this._vector = Vector<double>.Build.Dense(new double[6] { idx1, idx2, idx3, idx4, idx5, idx6 });
        }

        public IVec6D Cross(IVec6D vec3)
        {
            var vc = new Vec6D_base();
            vc[0] = this.Vector[1] * vec3.Vector[2] - this.Vector[2] * vec3.Vector[1];
            vc[1] = this.Vector[2] * vec3.Vector[0] - this.Vector[0] * vec3.Vector[2];
            vc[2] = this.Vector[0] * vec3.Vector[1] - this.Vector[1] * vec3.Vector[0];
            return vc;
        }

        public double Dot(IVec6D vec3)
        {
            return this._vector.DotProduct(vec3.Vector);
        }

        public double this[int idx]
        {
            get => this._vector[idx];
            set => this._vector[idx] = value;
        }

        public double L1Norm
        {
            get => this._vector.L1Norm();
        }

        public double L2Norm
        {
            get => this._vector.L2Norm();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public float[] ToArrary_F()
        {
            return new float[] {    Convert.ToSingle(this._vector[0]),
                                    Convert.ToSingle(this._vector[1]),
                                    Convert.ToSingle(this._vector[2]),
                                    Convert.ToSingle(this._vector[3]),
                                    Convert.ToSingle(this._vector[4]),
                                    Convert.ToSingle(this._vector[5]),
                                };
        }

        public double[] ToArrary()
        {
            return new double[] {
                this._vector[0],
                this._vector[1],
                this._vector[2],
                this._vector[3],
                this._vector[4],
                this._vector[5]
            };
        }
                

        #region operator
        public static Vec6D_base operator +(Vec6D_base left, Vec6D_base right)
        {
            return new Vec6D_base() { _vector = left._vector + right._vector };
        }
        public static Vec6D_base operator +(Vec6D_base left, double right)
        {
            return new Vec6D_base() { _vector = left._vector + right };
        }
        public static Vec6D_base operator +(double left, Vec6D_base right)
        {
            return new Vec6D_base() { _vector = left + right._vector };
        }
        public static Vec6D_base operator -(Vec6D_base left, Vec6D_base right)
        {
            return new Vec6D_base() { _vector = left._vector - right._vector };
        }
        public static Vec6D_base operator -(Vec6D_base left, double right)
        {
            return new Vec6D_base() { _vector = left._vector - right };
        }
        public static Vec6D_base operator -(double left, Vec6D_base right)
        {
            return new Vec6D_base() { _vector = left - right._vector };
        }
        public static double operator *(Vec6D_base left, Vec6D_base right)
        {
            return left._vector * right._vector;
        }
        public static Vec6D_base operator *(Vec6D_base left, double right)
        {
            return new Vec6D_base() { _vector = left._vector * right };
        }
        public static Vec6D_base operator *(double left, Vec6D_base right)
        {
            return new Vec6D_base() { _vector = left * right._vector };
        }
        public static Vec6D_base operator /(Vec6D_base left, Vec6D_base right)
        {
            return new Vec6D_base() { _vector = left._vector / right._vector };
        }
        public static Vec6D_base operator /(Vec6D_base left, double right)
        {
            return new Vec6D_base() { _vector = left._vector / right };
        }
        public static Vec6D_base operator /(double left, Vec6D_base right)
        {
            return new Vec6D_base() { _vector = left / right._vector };
        }
        #endregion

    }

    /// <summary>
    /// XYZ座標表現用ベクトルクラス
    /// </summary>
    public class XYZ : Vec3D_base
    {
        public XYZ() : base() { }

        public XYZ(double i1,double i2, double i3) : base(i1, i2, i3) { }

        public double X
        {
            get => this._vector[0];
            set => this._vector[0] = value;
        }

        public double Y
        {
            get => this._vector[1];
            set => this._vector[1] = value;
        }

        public double Z
        {
            get => this._vector[2];
            set => this._vector[2] = value;
        }

        public static XYZ operator +(XYZ left, XYZ right)
        {
            return new XYZ() { _vector = left._vector + right._vector };
        }
        public static XYZ operator +(XYZ left, double right)
        {
            return new XYZ() { _vector = left._vector + right };
        }
        public static XYZ operator +(double left, XYZ right)
        {
            return new XYZ() { _vector = left + right._vector };
        }
        public static XYZ operator -(XYZ left, XYZ right)
        {
            return new XYZ() { _vector = left._vector - right._vector };
        }
        public static XYZ operator -(XYZ left, double right)
        {
            return new XYZ() { _vector = left._vector - right };
        }
        public static XYZ operator -(double left, XYZ right)
        {
            return new XYZ() { _vector = left - right._vector };
        }
        public static double operator *(XYZ left, XYZ right)
        {
            return left._vector * right._vector;
        }
        public static XYZ operator *(XYZ left, double right)
        {
            return new XYZ() { _vector = left._vector * right };
        }
        public static XYZ operator *(double left, XYZ right)
        {
            return new XYZ() { _vector = left * right._vector };
        }
        public static XYZ operator /(XYZ left, XYZ right)
        {
            return new XYZ() { _vector = left._vector / right._vector };
        }
        public static XYZ operator /(XYZ left, double right)
        {
            return new XYZ() { _vector = left._vector / right };
        }
        public static XYZ operator /(double left, XYZ right)
        {
            return new XYZ() { _vector = left / right._vector };
        }

    }

    /// <summary>
    /// Role, Pitch, Yaw 表現用ベクトルクラス
    /// </summary>
    public class RPY : Vec3D_base
    {
        public RPY() : base() { }

        public RPY(double i1, double i2, double i3) : base(i1, i2, i3) { }

        public double Role
        {
            get => this._vector[0];
            set => this._vector[0] = value;
        }

        public double Pitch
        {
            get => this._vector[1];
            set => this._vector[1] = value;
        }

        public double Yaw
        {
            get => this._vector[2];
            set => this._vector[2] = value;
        }

        public Mat3D_W RotateMat
        {
            get
            {
                var rmat = new Mat3D_W();
                rmat.Matrix = Cartesian.RPY2Rmat(Role, Pitch, Yaw);
                return rmat;
            }
            set
            {
                Cartesian.Rmat2RPY(value.Matrix, out double roll, out double pitch, out double yaw);
                this.Role = roll;
                this.Pitch = pitch;
                this.Yaw = yaw;
            }
        }

        public Vec3D_base Euler
        {
            get
            {
                Cartesian.RPY2Euler(this.Role, this.Pitch, this.Yaw, out double a, out double b, out double c);
                return new Vec3D_base(a, b, c);
            }
            set
            {
                Cartesian.Euler2RPY(value.Vector[0], value.Vector[1], value.Vector[2], out double roll, out double pitch, out double yaw);
                this.Role = roll;
                this.Pitch = pitch;
                this.Yaw = yaw;
            }
        }

        public static RPY operator +(RPY left, RPY right)
        {
            return new RPY() { _vector = left._vector + right._vector };
        }
        public static RPY operator +(RPY left, double right)
        {
            return new RPY() { _vector = left._vector + right };
        }
        public static RPY operator +(double left, RPY right)
        {
            return new RPY() { _vector = left + right._vector };
        }
        public static RPY operator -(RPY left, RPY right)
        {
            return new RPY() { _vector = left._vector - right._vector };
        }
        public static RPY operator -(RPY left, double right)
        {
            return new RPY() { _vector = left._vector - right };
        }
        public static RPY operator -(double left, RPY right)
        {
            return new RPY() { _vector = left - right._vector };
        }
        public static double operator *(RPY left, RPY right)
        {
            return left._vector * right._vector;
        }
        public static RPY operator *(RPY left, double right)
        {
            return new RPY() { _vector = left._vector * right };
        }
        public static RPY operator *(double left, RPY right)
        {
            return new RPY() { _vector = left * right._vector };
        }
        public static RPY operator /(RPY left, RPY right)
        {
            return new RPY() { _vector = left._vector / right._vector };
        }
        public static RPY operator /(RPY left, double right)
        {
            return new RPY() { _vector = left._vector / right };
        }
        public static RPY operator /(double left, RPY right)
        {
            return new RPY() { _vector = left / right._vector };
        }
    }

    /// <summary>
    /// ロボット座標表現クラス
    /// </summary>
    public class Pose: Vec6D_base
    {
        public Pose() : base()
        {

        }

        public Pose(double idx1, double idx2, double idx3, double idx4, double idx5, double idx6) : base(idx1, idx2, idx3, idx4, idx5, idx6)
        {

        }
        
        public Pose(XYZ xyz, RPY rpy)
        {
            this._vector = Vector<double>.Build.Dense(new double[6] { xyz.X, xyz.Y, xyz.Z, rpy.Role, rpy.Pitch, rpy.Yaw });
        }

        public XYZ XYZ
        {
            get => new XYZ(this._vector[0], this._vector[1], this._vector[2]);
            set
            {
                this._vector[0] = value.X;
                this._vector[1] = value.Y;
                this._vector[2] = value.Z;

            }
        }

        public RPY RPY
        {
            get => new RPY(this._vector[3], this._vector[4], this._vector[5]);
            set
            {
                this._vector[3] = value.Role;
                this._vector[4] = value.Pitch;
                this._vector[5] = value.Yaw;

            }
        }

        public double X
        {
            get => this._vector[0];
            set => this._vector[0] = value;
        }

        public double Y
        {
            get => this._vector[1];
            set => this._vector[1] = value;
        }

        public double Z
        {
            get => this._vector[2];
            set => this._vector[2] = value;
        }

        public double Role
        {
            get => this._vector[3];
            set => this._vector[3] = value;
        }

        public double Pitch
        {
            get => this._vector[4];
            set => this._vector[4] = value;
        }

        public double Yaw
        {
            get => this._vector[5];
            set => this._vector[5] = value;
        }

        #region operator
        public static Pose operator +(Pose left, Pose right)
        {
            return new Pose() { _vector = left._vector + right._vector };
        }
        public static Pose operator +(Pose left, double right)
        {
            return new Pose() { _vector = left._vector + right };
        }
        public static Pose operator +(double left, Pose right)
        {
            return new Pose() { _vector = left + right._vector };
        }
        public static Pose operator -(Pose left, Pose right)
        {
            return new Pose() { _vector = left._vector - right._vector };
        }
        public static Pose operator -(Pose left, double right)
        {
            return new Pose() { _vector = left._vector - right };
        }
        public static Pose operator -(double left, Pose right)
        {
            return new Pose() { _vector = left - right._vector };
        }
        public static double operator *(Pose left, Pose right)
        {
            return left._vector * right._vector;
        }
        public static Pose operator *(Pose left, double right)
        {
            return new Pose() { _vector = left._vector * right };
        }
        public static Pose operator *(double left, Pose right)
        {
            return new Pose() { _vector = left * right._vector };
        }
        public static Pose operator /(Pose left, Pose right)
        {
            return new Pose() { _vector = left._vector / right._vector };
        }
        public static Pose operator /(Pose left, double right)
        {
            return new Pose() { _vector = left._vector / right };
        }
        public static Pose operator /(double left, Pose right)
        {
            return new Pose() { _vector = left / right._vector };
        }
        #endregion
    }

    /// <summary>
    /// ベクトル計算系の拡張クラス
    /// </summary>
    public static class TensorEX
    {
        public static Pose Join(this Vec3D_base vec3, RPY rpy)
        {
            XYZ xyz = new XYZ(vec3[0], vec3[1], vec3[2]);
            return new Pose(xyz, rpy);
        }

        public static Pose Join(this Vec3D_base vec3, XYZ xyz)
        {
            RPY rpy = new RPY(vec3[0], vec3[1], vec3[2]);
            return new Pose(xyz, rpy);
        }

        public static Pose Join(this XYZ xyz, RPY rpy)
        {
            return new Pose(xyz, rpy);
        }

        public static Pose Join(this RPY rpy, XYZ xyz)
        {
            return new Pose(xyz, rpy);
        }

        public static XYZ Rotate(this XYZ _xyz, Mat3D_W mat3D)
        {
            var mat3 = mat3D.Matrix;
            var vec3 = _xyz.Vector;
            var ans = mat3 * vec3;
            return new XYZ(ans[0], ans[1], ans[2]);
        }

        public static Pose Rotate(this Pose pose, Mat3D_W rotateMat)
        {
            Pose buf = new Pose();
            buf.XYZ = pose.XYZ.Rotate(rotateMat);
            buf.RPY = pose.RPY;
            return buf;
        }

        public static RPY Rotate(this RPY rpy, Mat3D_W mat3D)
        {
            return (mat3D * rpy.RotateMat).RPY;
        }
                
    }
}

    

    
