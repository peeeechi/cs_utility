﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Core;
using MongoDB.Driver.Wrappers;
using MongoDB.Driver.GeoJsonObjectModel;

namespace Commons
{
    

    /// <summary>
    /// MongoDB使用のためのクラス
    /// </summary>
    /// <param name="IP">データベースのIPアドレス:PC本体の場合はlocalhost</param> 
    class UseMongoDB
    {
        public class DBInfo
        {
            /// <summary>
            /// 接続先のIPアドレス
            /// </summary>
            public string IP = "localhost";     
            /// <summary>
            /// 接続するデータベース名
            /// </summary>
            public string DatabaseName = ""; 
            /// <summary>
            /// 接続するコレクション名
            /// </summary>
            public string CollectionName = "";
        }

        /// <summary>
        /// 比較検索用条件
        /// </summary>
        public struct FilterType
        {
            /// <summary>
            ///超過
            /// </summary>
            public const string Over = "Gt";
            /// <summary>
            /// 以上
            /// </summary>
            public const string AndOver = "Gte";
            /// <summary>
            /// 同値
            /// </summary>
            public const string Equals = "Eq";
            /// <summary>
            /// 以下
            /// </summary>
            public const string AndLess = "Lte";
            /// <summary>
            /// 未満
            /// </summary>
            public const string Under = "Lt";
            /// <summary>
            /// ≠
            /// </summary>
            public const string NotEquals = "Ne";

            static public bool CheckEqual(string Type)
            {
                if (Type == Over||Type == AndOver||Type == Equals||Type == AndLess||Type == Under||Type == NotEquals)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// 比較検索用のフィルター構成要素
        /// </summary>
        public class FilterElement
        {
            /// <summary>
            /// 比較項目名
            /// </summary>
            public string Key;
            /// <summary>
            /// 比較値
            /// </summary>
            public object Value;
            /// <summary>
            /// 比較のタイプ:FilterTypeより選択
            /// </summary>
            public string Type = "";

            public FilterElement()
            {}
            public FilterElement(string Key, object Value, string Type)
            {
                this.Key = Key;
                this.Value = Value;
                if (FilterType.CheckEqual(Type) == true)
                {
                    this.Type = Type;
                }                
            }

            /// <summary>
            /// このクラス内の設定のフィルターを返す
            /// </summary>
            /// <typeparam name="T">検索されるデータの型</typeparam>
            /// <returns>フィルター</returns>
            public FilterDefinition<T> MakeThisFilter<T>()
            {
                if (this.Key == null || this.Key == "")
                {
                    return null;
                }
                if (this.Value == null)
                {
                    return null;
                }
                FilterDefinition<T> filter;

                switch (this.Type)
                {
                    #region
                    case (FilterType.Over):
                        #region
                        int buf = Convert.ToInt32(this.Value);
                        filter = Builders<T>.Filter.Gt(this.Key, buf);
                        #endregion
                        break;
                    case (FilterType.AndOver):
                        #region
                        filter = Builders<T>.Filter.Gte(this.Key, this.Value);
                        #endregion
                        break;
                    case (FilterType.Equals):
                        #region
                        filter = Builders<T>.Filter.Eq(this.Key, this.Value);
                        #endregion
                        break;
                    case (FilterType.AndLess):
                        #region
                        filter = Builders<T>.Filter.Lte(this.Key, this.Value);
                        #endregion
                        break;
                    case (FilterType.Under):
                        #region
                        filter = Builders<T>.Filter.Lt(this.Key, this.Value);
                        #endregion
                        break;
                    case (FilterType.NotEquals):
                        #region
                        filter = Builders<T>.Filter.Ne(this.Key, this.Value);
                        #endregion
                        break;
                    case (""):
                        #region
                        filter = Builders<T>.Filter.Empty;
                        #endregion
                        break;
                    default:
                        filter = Builders<T>.Filter.Empty;
                        break;
                    #endregion
                }
                return filter;
            }

        }

        public List<FilterElement> FilterList = new List<FilterElement>();

        /// <summary>
        /// データベースとの接続を保持
        /// </summary>
        public MongoClient client;

        double TimeOut_MSec = 10000;
//-----------------------------------------------------------------------------------------------------------------------------------------------------
//コンストラクタ
        /// <summary>
        /// クラスのインスタンス作成時にDBに接続(初期化処理)
        /// </summary>
        /// <param name="IP">接続するDBのIPアドレス</param>
        public UseMongoDB(string IP)
        {
            try
            {
                //クライアント接続
                client = new MongoClient(new MongoClientSettings 
                { 
                    Server = new MongoServerAddress(IP),
                    SocketTimeout = new TimeSpan(0,0,0,3),
                    WaitQueueTimeout = new TimeSpan(0, 0, 0, 3),
                    ConnectTimeout = new TimeSpan(0,0,0,3)
                });
                //client = new MongoClient("mongodb://" + IP);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                return;
            }
            
        }
        /// <summary>
        /// クラスのインスタンス作成時にDBに接続(初期化処理)
        /// </summary>
        /// <param name="IP">接続するDBのIPアドレス</param>
        /// <param name="TimeOut_MSec">タイムアウトさせる時間(ミリ秒)</param>
        public UseMongoDB(string IP, double TimeOut_MSec)
        {
            try
            {
                //クライアント接続
                client = new MongoClient(new MongoClientSettings
                {
                    Server = new MongoServerAddress(IP),
                    SocketTimeout = new TimeSpan(0, 0, 0, 3),
                    WaitQueueTimeout = new TimeSpan(0, 0, 0, 3),
                    ConnectTimeout = new TimeSpan(0, 0, 0, 3)
                });
                //client = new MongoClient("mongodb://" + IP);
                this.TimeOut_MSec = TimeOut_MSec;
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                return;
            }

        }
//-----------------------------------------------------------------------------------------------------------------------------------------------------     
//データ追加
        /// <summary>
        /// データベースへデータを挿入
        /// </summary>
        /// <typeparam name="T">挿入するデータのクラス</typeparam>
        /// <param name="IP">データベースのIPアドレス:PC本体の場合はlocalhost</param>
        /// <param name="DatabaseName">接続するデータベース名</param>
        /// <param name="CollectionName">取得するコレクション名</param>
        /// <param name="AddData">挿入するデータ</param>
        public void AddData<T>(string DatabaseName, string CollectionName, ref T AddData)
        {
            try
            {
                //データベース接続（作成）
                MongoDB.Driver.IMongoDatabase db = client.GetDatabase(DatabaseName);
                //コレクション作成(コレクション形式指定)
                MongoDB.Driver.IMongoCollection<T> col = db.GetCollection<T>(CollectionName);
                //データベースへデータ挿入
                
                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(1500)))
                {
                    col.InsertOne(AddData,null,timeOut.Token);
                }
                
                
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                return;
            }
            
        }
//-----------------------------------------------------------------------------------------------------------------------------------------------------
//データ検索取得
        /// <summary>
        /// filterにマッチしたドキュメントを返す
        /// </summary>
        /// <param name="DatabaseName">検索するデータベース名</param>
        /// <param name="CollectionName">検索するコレクション名</param>
        /// <param name="filter">{{検索項目,検索値}}</param>
        /// <returns>検索にマッチしたドキュメントのリスト</returns>
        public List<BsonDocument> SearchData(string DatabaseName, string CollectionName,BsonDocument filter)
        {
            try
            {
                MongoDB.Driver.IMongoDatabase db = client.GetDatabase(DatabaseName);
                MongoDB.Driver.IMongoCollection<BsonDocument> col = db.GetCollection<BsonDocument>(CollectionName);
                

                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    var cursor = col.Find(filter).ToList(timeOut.Token);
                    return cursor;
                }
            }
            catch (Exception)
            {
                //MessageBox.Show("通信異常!!\n通信設定を確認してください");
                return null;
            }
            
        }
        /// <summary>
        /// filterにマッチしたドキュメントを返す
        /// </summary>
        /// <param name="DatabaseName">検索するデータベース名</param>
        /// <param name="CollectionName">検索するコレクション名</param>
        /// <param name="filter">{{検索項目,検索値}}</param>
        /// <returns>検索にマッチしたドキュメントのリスト</returns>
        public List<BsonDocument> SearchData(string DatabaseName, string CollectionName, MongoDB.Driver.FilterDefinition<BsonDocument> filter)
        {
            try
            {
                MongoDB.Driver.IMongoDatabase db = client.GetDatabase(DatabaseName);
                MongoDB.Driver.IMongoCollection<BsonDocument> col = db.GetCollection<BsonDocument>(CollectionName);


                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    var cursor = col.Find(filter).ToList(timeOut.Token);
                    return cursor;
                }
            }
            catch (Exception)
            {
                //MessageBox.Show("通信異常!!\n通信設定を確認してください");
                return null;
            }

        }

        /// <summary>
        /// filterにマッチしたドキュメントを返す(データ形式指定版)
        /// </summary>
        /// <typeparam name="T">検索するデータの型</typeparam>
        /// <param name="DatabaseName">検索するデータベース名</param>
        /// <param name="CollectionName">検索するコレクション名</param>
        /// <param name="filter">{{検索項目,検索値}}</param>
        /// <returns></returns>
        public List<T> SearchData<T>(string DatabaseName, string CollectionName, BsonDocument filter)
        {
            List<T> GetDatas = new List<T>();

            try
            {
                //データベース接続（作成）
                MongoDB.Driver.IMongoDatabase db = client.GetDatabase(DatabaseName);
                //コレクション定義
                MongoDB.Driver.IMongoCollection<T> col = db.GetCollection<T>(CollectionName);

                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    GetDatas = col.Find(filter).ToList(timeOut.Token);
                    return GetDatas;
                }
                
            }
            catch (Exception)
            {
                // MessageBox .Show("通信異常!!\n通信設定を確認してください");
                return null;
            }

        }
        /// <summary>
        /// filterにマッチしたドキュメントを返す(データ形式指定版)
        /// </summary>
        /// <typeparam name="T">検索するデータの型</typeparam>
        /// <param name="DatabaseName">検索するデータベース名</param>
        /// <param name="CollectionName">検索するコレクション名</param>
        /// <param name="filter">{{検索項目,検索値}}</param>
        /// <returns></returns>
        public List<T> SearchData<T>(string DatabaseName, string CollectionName, MongoDB.Driver.FilterDefinition<T> filter)
        {
            List<T> GetDatas = new List<T>();

            try
            {
                //データベース接続（作成）
                MongoDB.Driver.IMongoDatabase db = client.GetDatabase(DatabaseName);
                //コレクション定義
                MongoDB.Driver.IMongoCollection<T> col = db.GetCollection<T>(CollectionName);

                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    GetDatas = col.Find(filter).ToList(timeOut.Token);
                    return GetDatas;
                }

            }
            catch (Exception)
            {
                // MessageBox .Show("通信異常!!\n通信設定を確認してください");
                return null;
            }

        }
        /// <summary>
        /// filterに最初にマッチしたドキュメントを返す(データ形式指定版)
        /// </summary>
        /// <typeparam name="T">検索するデータの型</typeparam>
        /// <param name="DatabaseName">検索するデータベース名</param>
        /// <param name="CollectionName">検索するコレクション名</param>
        /// <param name="filter">{{検索項目,検索値}}</param>
        /// <returns>マッチしたデータ</returns>
        public T SearchDataFirst<T>(string DatabaseName, string CollectionName, BsonDocument filter)
        {

            try
            {
                //データベース接続（作成）
                MongoDB.Driver.IMongoDatabase db = client.GetDatabase(DatabaseName);
                //コレクション定義
                MongoDB.Driver.IMongoCollection<T> col = db.GetCollection<T>(CollectionName);

                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    T GetData = col.Find(filter).First();
                    return GetData;
                }
            }
            catch (Exception e)
            {
                // MessageBox .Show(e.Message);
                return default(T);
            }

        }
//-----------------------------------------------------------------------------------------------------------------------------------------------------
//フィルター作成
        /// <summary>
        /// 渡した条件をＡＮＤ条件で成立させる
        /// フィルターを返す
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="FilterList"></param>
        /// <returns></returns>
        public FilterDefinition<T> makeAndFilter<T>(List<FilterElement> FilterList)
        {
            MongoDB.Driver.FilterDefinition<T> filter;
            List<FilterDefinition<T>> makingFilterList = new List<FilterDefinition<T>>();

            try
            {
                foreach (var item in FilterList)
                {
                    MongoDB.Driver.FilterDefinition<T> buf = item.MakeThisFilter<T>();
                    if (makingFilterList.Contains(buf) != true)
                    {
                        makingFilterList.Add(buf);
                    }
                }

                filter = Builders<T>.Filter.And(makingFilterList);

                return filter;
            }
            catch (Exception error)
            {
                // MessageBox .Show(error.Message);
                return null;
            }        
        }
        /// <summary>
        /// 渡した条件をＡＮＤ条件で成立させる
        /// フィルターを返す
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="FilterList"></param>
        /// <returns></returns>
        public FilterDefinition<T> makeAndFilter<T>(MongoDB.Driver.FilterDefinition<T> filter1, MongoDB.Driver.FilterDefinition<T> filter2)
        {            
            MongoDB.Driver.FilterDefinition<T> filter;
            try
            {
                filter = Builders<T>.Filter.And(filter1,filter2);
                return filter;
            }
            catch (Exception error)
            {
                // MessageBox .Show(error.Message);
                return null;
            }
        }
        /// <summary>
        /// 渡した条件をＡＮＤ条件で成立させる
        /// フィルターを返す
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="FilterList"></param>
        /// <returns></returns>
        public FilterDefinition<T> makeAndFilter<T>(MongoDB.Driver.FilterDefinition<T> filter1, FilterElement elm)
        {
            MongoDB.Driver.FilterDefinition<T> filter;
            try
            {
                MongoDB.Driver.FilterDefinition<T> buf = elm.MakeThisFilter<T>();
                filter = Builders<T>.Filter.And(filter1, buf);
                return filter;
            }
            catch (Exception error)
            {
                // MessageBox .Show(error.Message);
                return null;
            }
        }
        

        /// <summary>
        /// 渡した条件をOR条件で成立させる
        /// フィルターを返す
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="FilterList"></param>
        /// <returns></returns>
        public FilterDefinition<T> makeOrFilter<T>(List<FilterElement> FilterList)
        {
            MongoDB.Driver.FilterDefinition<T> filter;
            List<FilterDefinition<T>> makingFilterList = new List<FilterDefinition<T>>();

            try
            {
                foreach (var item in FilterList)
                {
                    MongoDB.Driver.FilterDefinition<T> buf = item.MakeThisFilter<T>();
                    if (makingFilterList.Contains(buf) != true)
                    {
                        makingFilterList.Add(buf);
                    }
                }

                filter = Builders<T>.Filter.Or(makingFilterList);

                return filter;
            }
            catch (Exception error)
            {
                // MessageBox .Show(error.Message);
                return null;
            }
        }
        /// <summary>
        /// 渡した条件をOR条件で成立させる
        /// フィルターを返す
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="FilterList"></param>
        /// <returns></returns>
        public FilterDefinition<T> makeOrFilter<T>(MongoDB.Driver.FilterDefinition<T> filter1, MongoDB.Driver.FilterDefinition<T> filter2)
        {
            MongoDB.Driver.FilterDefinition<T> filter;
            try
            {
                filter = Builders<T>.Filter.Or(filter1, filter2);
                return filter;
            }
            catch (Exception error)
            {
                // MessageBox .Show(error.Message);
                return null;
            }
        }
        /// <summary>
        /// 渡した条件をOR条件で成立させる
        /// フィルターを返す
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="FilterList"></param>
        /// <returns></returns>
        public FilterDefinition<T> makeOrFilter<T>(MongoDB.Driver.FilterDefinition<T> filter1, FilterElement elm)
        {
            MongoDB.Driver.FilterDefinition<T> filter;
            try
            {
                MongoDB.Driver.FilterDefinition<T> buf = elm.MakeThisFilter<T>();
                filter = Builders<T>.Filter.Or(filter1, buf);
                return filter;
            }
            catch (Exception error)
            {
                // MessageBox .Show(error.Message);
                return null;
            }
        }

//-----------------------------------------------------------------------------------------------------------------------------------------------------
//全件取得 
        /// <summary>
        /// 指定した型のリストでデータを取得する
        /// (格納されているデータ構造がわかっている場合に使用)
        /// </summary>
        /// <typeparam name="T">取得するデータの型</typeparam>
        /// <param name="DatabaseName">接続するデータベース名</param>
        /// <param name="CollectionName">接続するコレクション名</param>
        /// <param name="GetDatas">取得してくるデータのリスト</param>
        public void Get_ALL_Typed_Data<T>(string DatabaseName, string CollectionName, ref List<T> GetDatas)
        {
            try
            {
                //データベース接続（作成）
                
                MongoDB.Driver.IMongoDatabase db = client.GetDatabase(DatabaseName);
                //コレクション定義
                MongoDB.Driver.IMongoCollection<T> col = db.GetCollection<T>(CollectionName);

                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    var filter = new BsonDocument(); //全て取得
                    GetDatas = col.Find(filter).ToList(timeOut.Token); 
                }
                
            }
            catch (Exception e)
            {
                // MessageBox .Show(e.Message);
                return;
            }                    
            
        }
        
        /// <summary>
        /// 指定したコレクションの全ドキュメントをBsonDocument形式で取得する
        /// </summary>
        /// <param name="DatabaseName"></param>
        /// <param name="CollectionName"></param>
        /// <returns>取得したデータのリスト</returns>
        public List<BsonDocument> Get_ALL_UnTyped_Data(string DatabaseName, string CollectionName)
        {
            try
            {
                MongoDB.Driver.IMongoDatabase db = client.GetDatabase(DatabaseName);
                MongoDB.Driver.IMongoCollection<BsonDocument> col = db.GetCollection<BsonDocument>(CollectionName);

                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    var cursor = col.Find(new BsonDocument()).ToList(timeOut.Token);
                    return cursor;
                }
                
            }
            catch (Exception e)
            {
                // MessageBox .Show(e.Message);
                return null;
            }
        }
//-----------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// filterにマッチしたドキュメントのkeyの項目をvalueの値で上書きする。なければ追加
        /// </summary>
        /// <param name="DatabaseName">接続するデータベース名</param>
        /// <param name="CollectionName">接続するコレクション名</param>
        /// <param name="filter">アップデートするドキュメントの検索条件</param>
        /// <param name="key">更新する項目</param>
        /// <param name="value">更新する値</param>
        /// <returns>更新した数</returns>
        public long UpdateDoc(string DatabaseName, string CollectionName, BsonDocument filter,string key,object value)
        {
            try
            {
                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    var db = client.GetDatabase(DatabaseName);
                    var col = db.GetCollection<BsonDocument>(CollectionName);

                    var upd = Builders<BsonDocument>.Update.Set(key, value);

                    var result = col.UpdateMany(filter, upd,null,timeOut.Token);
                    return result.ModifiedCount;
                }
            }
            catch (Exception e)
            {
                // MessageBox .Show(e.Message);
                return 0;
            }
            
        }

        /// <summary>
        /// filterにマッチしたドキュメントのkeyの項目をvalueの値で上書きする。なければ追加
        /// </summary>
        /// <param name="DatabaseName">接続するデータベース名</param>
        /// <param name="CollectionName">接続するコレクション名</param>
        /// <param name="filter">アップデートするドキュメントの検索条件</param>
        /// <param name="insertKey">追加する項目名</param>
        /// <param name="insertData">追加する項目の値</param>
        /// <returns>更新した数</returns>
        public long UpdateDoc(string DatabaseName, string CollectionName, BsonDocument filter,string insertKey,BsonDocument insertData)
        {
            try
            {
                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    var db = client.GetDatabase(DatabaseName);
                    var col = db.GetCollection<BsonDocument>(CollectionName);

                    var upd = Builders<BsonDocument>.Update.Set(insertKey, insertData);
                    var result = col.UpdateMany(filter, upd,null,timeOut.Token);

                    return result.ModifiedCount;
                }
            }
            catch (Exception e)
            {
                // MessageBox .Show(e.Message);
                return 0;
            }

        }
//-----------------------------------------------------------------------------------------------------------------------------------------------------
        public bool DeleteDatabase(string DatabaseName)
        {
            try
            {

                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    client.DropDatabase(DatabaseName);
                    return true;
                }
            }
            catch (Exception e)
            {
                // MessageBox .Show(e.Message);
                return false;
            }
        }
        public bool DeleteCollection(string DatabaseName, string CollectionName)
        {
            try
            {
                var db = client.GetDatabase(DatabaseName);

                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    db.DropCollection(CollectionName);

                    return true;
                }
            }
            catch (Exception e)
            {
                // MessageBox .Show(e.Message);
                return false;
            }
        }
        /// <summary>
        /// filterにマッチしたドキュメントを削除する
        /// </summary>
        /// <param name="DatabaseName">検索するデータベース名</param>
        /// <param name="CollectionName">検索するコレクション名</param>
        /// <param name="filter">検索条件</param>
        /// <returns>削除した数</returns>
        public long DeleteDoc(string DatabaseName, string CollectionName, BsonDocument filter) 
        {
            try
            {
                var db = client.GetDatabase(DatabaseName);
                var col = db.GetCollection<BsonDocument>(CollectionName);

                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    var result = col.DeleteMany(filter, timeOut.Token);
                    return result.DeletedCount;
                }
            }
            catch (Exception e)
            {
                // MessageBox .Show(e.Message);
                return 0;
            }
        }
//-----------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// データベース一覧を取得する
        /// </summary>
        /// <returns>取得したデータベース名の一覧</returns>
        public List<string> Get_DataBase_Name()
        {
            try
            {
                List<BsonDocument> list = new List<BsonDocument>();
                List<string> DB_Names = new List<string>();

                using (var cursor = client.ListDatabases())
                {
                    using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                    {
                        list = cursor.ToList(timeOut.Token);

                        for (int i = 0; i < list.Count; i++)
                        {
                            DB_Names.Add(list[i]["name"].AsString);
                        }
                    }                    
                }

                return DB_Names;
            }
            catch (Exception e)
            {
                // MessageBox .Show(e.Message);
                return null;
            }
            
            
        }
        /// <summary>
        /// 指定したデータベース内のコレクション一覧を取得する
        /// </summary>
        /// <param name="DatabaseName">検索するデータベース名</param>
        /// <returns>取得したコレクション名の一覧</returns>
        public List<string> Get_Collections_Name(string DatabaseName)
        {
            try
            {
                List<string> Collections_Name = new List<string>();

                var db = client.GetDatabase(DatabaseName);

                using (var cursor = db.ListCollections())
                {
                    using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                    {
                        List<BsonDocument> list = cursor.ToList(timeOut.Token); // do something with the list                  
                        for (int i = 0; i < list.Count; i++)
                        {
                            Collections_Name.Add(list[i]["name"].AsString);
                        }
                    }    
                    
                }

                return Collections_Name;
            }
            catch (Exception e)
            {
                // MessageBox .Show(e.Message);
                return null;
            }
            
        }
        /// <summary>
        /// 指定したコレクション内のドキュメント一覧を取得する
        /// </summary>
        /// <param name="DatabaseName">検査するデータベース名</param>
        /// <param name="CollectionName">検査するコレクション名</param>
        /// <returns>取得したドキュメント名の一覧</returns>
        public List<List<string>> Get_Document_Name(string DatabaseName, string CollectionName)
        {
            try
            {
                List<List<string>> DocumentNameS = new List<List<string>>();

                MongoDB.Driver.IMongoDatabase db = client.GetDatabase(DatabaseName);
                MongoDB.Driver.IMongoCollection<BsonDocument> col = db.GetCollection<BsonDocument>(CollectionName);
                var filter = new BsonDocument();
                using (var timeOut = new System.Threading.CancellationTokenSource(TimeSpan.FromMilliseconds(TimeOut_MSec)))
                {
                    var cursor = col.Find(filter).ToList(timeOut.Token);

                    for (int i = 0; i < cursor.Count; i++)
                    {
                        List<string> DocumentName = new List<string>();
                        List<BsonElement> list = cursor[i].Elements.ToList();
                        for (int k = 0; k < list.Count; k++)
                        {
                            DocumentName.Add(list[k].Name);
                        }
                        DocumentNameS.Add(DocumentName);

                    }

                    return DocumentNameS;
                }
            }
            catch (Exception e)
            {
                // MessageBox .Show(e.Message);
                return null;
            }

            

        }
    }
}
