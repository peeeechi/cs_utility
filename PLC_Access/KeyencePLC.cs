﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using static Utility.Util;
using Utility;

namespace PLC_Access.KV7500
{
    /// <summary>
    /// エラー発生時にコールバックされる関数ポインタ
    /// </summary>
    /// <param name="param">関数の引数</param>
    public delegate void AfterErrFunc(string ErrorCode);

    /// <summary>
    /// 通信タイプ
    /// </summary>
    public enum ConnectorType
    {
        /// <summary>
        /// TCP通信
        /// </summary>
        TCP = 0,
        /// <summary>
        /// Serial通信
        /// </summary>
        Serial
    }

    /// <summary>
    /// PLCのメッセージ送信に使用する通信クラスのインターフェイスを規定します
    /// </summary>
    public interface IUpperLinkConnector
    {
        /// <summary>
        /// 接続処理
        /// </summary>
        void Connect();
        /// <summary>
        /// メッセージを送信します
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        string Send(string message);
        /// <summary>
        /// 接続を切断します
        /// </summary>
        void DisConnect();
        /// <summary>
        /// 初期化処理を行います
        /// </summary>
        /// <param name="file"></param>
        void Init(string file);
        /// <summary>
        /// 設定を保存します
        /// </summary>
        /// <param name="file"></param>
        void Save(string file);
        /// <summary>
        /// 接続中かどうかを取得します
        /// </summary>
        bool IsConnect
        {
            get;
        }
        /// <summary>
        /// 通信タイプを取得します
        /// </summary>
        ConnectorType ConnectType
        {
            get;
        }
        /// <summary>
        /// 通信に使用するエンコーディングの取得、設定をします
        /// </summary>
        Encoding Encoding
        {
            get;
            set;
        }
        /// <summary>
        /// 送信可能かどうかを取得します
        /// </summary>
        bool IsSendable
        {
            get;
        }
        /// <summary>
        /// 通信タイムアウトを設定します
        /// </summary>
        int TimeOut
        {
            get;set;
        }
    }

    /// <summary>
    /// TCP_IPプロトコルで通信処理を行う
    /// </summary>
    public class TCP_ConUnit : IUpperLinkConnector
    {
        private TCP_IP_Connect.Client client;        

        private Encoding _encoding = Encoding.UTF8;

        private string _ip;
        private bool _IsSendable = false;
        private int _SendPort;
        private int _RecivePort;

        /// <summary>
        /// 送信文字列のエンコーディング
        /// </summary>
        public Encoding Encoding
        {
            get => this._encoding;
            set => this._encoding = value;
        }

        /// <summary>
        /// 通信が接続状態かどうか
        /// </summary>
        public bool IsConnect
        {
            get => this.client.IsConnect;
        }

        /// <summary>
        /// 通信タイプ
        /// </summary>
        public ConnectorType ConnectType
        {
            get => ConnectorType.TCP;
        }

        /// <summary>
        /// 送信元のPortNoを取得、設定します
        /// </summary>
        public int SendPort
        {
            get => this.client.SendPort;
            set
            {
                this.client.SendPort = value;
                _SendPort = value;
            }
        }

        /// <summary>
        /// 受信元のPortNoを取得、設定します
        /// </summary>
        public int RecivePort
        {
            get => this.client.RecivePort;
            set
            {
                this.client.RecivePort = value;
                this._RecivePort = value;
            }
        }

        /// <summary>
        /// 送信可能かを取得します True:可能
        /// </summary>
        public bool IsSendable
        {
            get => this._IsSendable;
        }
        /// <summary>
        /// 通信タイムアウトを設定します
        /// </summary>
        public int TimeOut
        {
            get => this.client.WriteTimeOut;
            set
            {
                var timeout                 = (value > 0) ? value : System.Threading.Timeout.Infinite;
                this.client.WriteTimeOut    = timeout;
                this.client.ReadTimeOut     = timeout;
            }
        }

        public TCP_ConUnit()
        {
            this.client = new TCP_IP_Connect.Client();
            SetIP_Port(this.client.IP, this.client.SendPort);
        }

        public TCP_ConUnit(string ip)
        {
            this.client = new TCP_IP_Connect.Client(ip);
            SetIP_Port(this.client.IP, this.client.SendPort);
        }

        public TCP_ConUnit(int sendPort)
        {
            this.client = new TCP_IP_Connect.Client(sendPort);
            SetIP_Port(this.client.IP, this.client.SendPort);
        }

        public TCP_ConUnit(string ip, int sendPort)
        {
            this.client = new TCP_IP_Connect.Client(ip, sendPort);
            SetIP_Port(this.client.IP, this.client.SendPort);
        }

        public TCP_ConUnit(string ip, int sendPort, int recivePort)
        {
            this.client = new TCP_IP_Connect.Client(ip, sendPort);
            SetIP_Port(this.client.IP, this.client.SendPort, recivePort);
        }

        /// <summary>
        /// IPとPortNoの初期化
        /// </summary>
        /// <param name="ip">IPアドレス</param>
        /// <param name="sendPort">送信先PortNo</param>
        /// <param name="recivePort">受信先PortNo</param>
        private void SetIP_Port(string ip, int sendPort, int recivePort = -1)
        {
            this.client.Send_EndCode = "\r";
            this.client.Res_EndCode = "\r\n";
            this._ip       = ip;
            this.SendPort  = sendPort;
            if (recivePort != -1)
            {
                this.RecivePort = recivePort;
            }
        }        

        /// <summary>
        /// 接続処理
        /// </summary>
        /// <returns></returns>
        public void Connect()
        {
            if (this.client == null)
            {
                this.client = new TCP_IP_Connect.Client(this._ip, this._SendPort);
                this.client.Send_EndCode = "\r";
                this.client.Res_EndCode = "\r\n";
            }
            this.client.Connect();
        }

        public void Init(string file)
        {
            //var param = this.client.param;
            var param = ReadFile(file, this._encoding).JsonStrToObj<TCP_IP_Connect.Client.Param>();
            this.client.param = param;
        }

        public void Save(string file)
        {
            this.client.param.JsonDumpStr().FileDump(file, this._encoding);
        }

        /// <summary>
        /// 通信切断処理
        /// </summary>
        public void DisConnect()
        {
            this.client.DisConnect();
            this.client = null;
        }

        /// <summary>
        /// メッセージを送信します
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string Send(string message)
        {
            this._IsSendable = false;
            string res = this.client.Send(message);
            this._IsSendable = true;

            return res;
        }
    }

    /// <summary>
    /// Serial通信処理を行う
    /// </summary>
    public class Serial_ConUnit : IUpperLinkConnector
    {
        private SerialConnector serial;
        private SerialConnector.Param _serialParam;
        private Encoding _encoding = Encoding.UTF8;
        private bool _IsSendable = false;

        /// <summary>
        /// 送信文字列のエンコーディング
        /// </summary>
        public Encoding Encoding
        {
            get => this._encoding;
            set
            {
                this._encoding = value;
                this._serialParam.encoding = value.EncodingName;
            }
        }

        /// <summary>
        /// 接続状態を取得する
        /// </summary>
        public bool IsConnect { get => this.serial.IsConnect; }

        public ConnectorType ConnectType { get => ConnectorType.Serial; }

        /// <summary>
        /// 送信可能かを取得します True:可能
        /// </summary>
        public bool IsSendable
        {
            get => this._IsSendable;
        }
        /// <summary>
        /// 通信タイムアウトを設定します
        /// </summary>
        public int TimeOut
        {
            get => this.serial.WriteTimeout;
            set
            {
                var timeout = (value > 0) ? value : System.Threading.Timeout.Infinite;
                this.serial.WriteTimeout = timeout;
                this.serial.ReadTimeout = timeout;
            }
        }

        public Serial_ConUnit()
        {
            this._serialParam = new SerialConnector.Param();
            this._serialParam.Res_EndCode = "\r\n";
            this._serialParam.Send_EndCode = "\r";
            this.serial = new SerialConnector(this._serialParam.ComPort);
            this.serial.Init(this._serialParam);
        }

        public Serial_ConUnit(string comPort)
        {
            this._serialParam = new SerialConnector.Param();
            this._serialParam.Res_EndCode = "\r\n";
            this._serialParam.Send_EndCode = "\r";
            this._serialParam.ComPort = comPort;
            this.serial = new SerialConnector(this._serialParam.ComPort);
            this.serial.Init(this._serialParam);
        }

        public void Init(string file)
        {
            var param = ReadFile(file, this._encoding).JsonStrToObj<SerialConnector.Param>();
            this.serial.Init(param);
        }

        public void Save(string file)
        {
            this.serial.param.JsonDumpStr().FileDump(file, this._encoding);
        }

        /// <summary>
        /// 通信接続処理を行う
        /// </summary>
        public void Connect()
        {
            if (this.serial == null)
            {
                this.serial = new SerialConnector(this._serialParam.ComPort);
                this.serial.Init(this._serialParam);
            }
            this.serial.Connect();
            var res = this.serial.Send("CR").Split('\r')[0];

            if (res != "CC")
            {
                throw new Exception(res);
            }
        }

        /// <summary>
        /// 通信切断処理を行う
        /// </summary>
        public void DisConnect()
        {
            var res = this.serial.Send("CQ").Split('\r')[0];

            if (res != "CF")
            {
                throw new Exception(res);
            }
            this.serial.DisConnect();
            this.serial = null;
        }

        /// <summary>
        /// メッセージを送信します
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string Send(string message)
        {
            this._IsSendable = false;
            var res = this.serial.Send(message);
            this._IsSendable = true;
            return res;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum Switch
    {
        ON = 0,
        OFF = 1
    }

    /// <summary>
    /// 
    /// </summary>
    public enum Suffix
    {
        /// <summary>
        /// 指定なし（デフォルトの設定を使用する）
        /// </summary>
        None = -1,
        /// <summary>
        /// 10進数16ビット符号なし
        /// </summary>
        U = 0,
        /// <summary>
        /// 10進数16ビット符号あり
        /// </summary>
        S = 1,
        /// <summary>
        /// 10進数32ビット符号なし
        /// </summary>
        D = 2,
        /// <summary>
        /// 10進数32ビット符号あり
        /// </summary>
        L = 3,
        /// <summary>
        /// 16進数16ビット
        /// </summary>
        H = 4
    }


    #region DataReadWriteBase Template RD,RDS,WR,WRSコマンド用の Template 

    /// <summary>
    /// RD,RDS,WR,WRSコマンド用の Template 
    /// </summary>
    public abstract class DataReadWriteBase
    {

        /// <summary>
        /// Suffix（Enum）を文字列へ変換する
        /// </summary>
        private static readonly Dictionary<Suffix, string> DictSuffix = new Dictionary<Suffix, string>()
        {
            { Suffix.None,  ""      },
            { Suffix.U,     ".U"    },
            { Suffix.S,     ".S"    },
            { Suffix.D,     ".D"    },
            { Suffix.L,     ".L"    },
            { Suffix.H,     ".H"    }
        };

        private Func<string, string> SendMethod;
        private Suffix _suffix = Suffix.None;
        /// <summary>
        /// 
        /// </summary>
        public Suffix Suffix
        {
            get => this._suffix;
            set => this._suffix = value;
        }

        /// <summary>
        /// DMなどのコマンドヘッダーを登録する
        /// </summary>
        protected string _header;

        public DataReadWriteBase(string header)
        {
            this._header = header;
        }

        /// <summary>
        /// 通信するためのメソッドを登録します
        /// </summary>
        /// <param name="funcPtr"></param>
        public void SetFunction(Func<string, string> funcPtr)
        {
            this.SendMethod = funcPtr;
        }

        /// <summary>
        /// 単一のデバイスへの値の設定、取得を行います(サフィックス自動設定)
        /// </summary>
        /// <param name="deviceNo">設定するデバイスNo</param>
        /// <returns>取得した値</returns>
        public string this[int deviceNo]
        {
            get
            {
                //string mes = String.Concat("RD ", this._header, deviceNo.ToString());
                string mes = String.Concat("RD ", this._header, deviceNo.ToString(), DictSuffix[this._suffix]);
                string res = this.SendMethod(mes);

                return res;
            }
            set
            {
                Suffix suffix;
                if(value == "0" || value == "1")    //ビットのときはUで送る(数値と区別がつかないため)
                {
                    suffix = Suffix.U;
                }
                else if (ushort.TryParse(value, out ushort resultU))
                {
                    suffix = Suffix.U;
                }
                else if (short.TryParse(value, out short resultS))
                {
                    suffix = Suffix.S;
                }
                else if (uint.TryParse(value, out uint resultD))
                {
                    suffix = Suffix.D;
                }
                else if (int.TryParse(value, out int resultL))
                {
                    suffix = Suffix.L;
                }
                //string mes = String.Concat("WR ", this._header, deviceNo.ToString(), " ", value);
                string mes = String.Concat("WR ", this._header, deviceNo.ToString(), DictSuffix[this._suffix], " ", value);
                string res = this.SendMethod(mes);
            }
        }

        /// <summary>
        /// 単一のデバイスへの値の設定、取得を行います(サフィックス手動設定)
        /// </summary>
        /// <param name="deviceNo">設定するデバイスNo</param>
        /// <returns>取得した値</returns>
        public string this[int deviceNo, Suffix suffix]
        {
            get
            {
                //string mes = String.Concat("RD ", this._header, deviceNo.ToString());
                string mes = String.Concat("RD ", this._header, deviceNo.ToString(), DictSuffix[suffix]);
                string res = this.SendMethod(mes);

                return res;
            }
            set
            {
                //string mes = String.Concat("WR ", this._header, deviceNo.ToString(), " ", value);
                string mes = String.Concat("WR ", this._header, deviceNo.ToString(), DictSuffix[suffix], " ", value);
                string res = this.SendMethod(mes);
            }
        }

        /// <summary>
        /// 複数のデバイスへの値の取得、設定を行います
        /// </summary>
        /// <param name="deviceNo">先頭デバイス</param>
        /// <param name="num">設定データ数</param>
        /// <returns>取得した値</returns>
        public string[] this[int deviceNo, int num]
        {
            get
            {
                //string mes = String.Concat("RD ", this._header, deviceNo.ToString());
                string mes = String.Concat("RDS ", this._header, deviceNo.ToString(), DictSuffix[this._suffix], " ", num.ToString());
                string res = this.SendMethod(mes);

                return res.Split(new char[] { ' ' });
            }
            set
            {
                var datasStr = String.Join(" ", value);
                //string mes = String.Concat("WR ", this._header, deviceNo.ToString(), " ", value);
                string mes = String.Concat("WRS ", this._header, deviceNo.ToString(), DictSuffix[this._suffix], " ", value.Length.ToString(), " ", datasStr);
                string res = this.SendMethod(mes);
            }
        }
    }
    // RD   "RD DM1200.U"
    // RDS  "RDS DM1200.U 20"
    // WR   "WR DM1200.U 200"
    // WRS  "WR DM1200.U 5 20 200 29 120 33"
    /// <summary>
    /// データメモリ デバイスへ、値の取得、設定を行います
    /// </summary>
    public class DM_Class : DataReadWriteBase
    {
        public DM_Class() : base("DM") { }
    }
    /// <summary>
    /// リレー デバイスへ、値の取得、設定を行います
    /// </summary>
    public class R_Class : DataReadWriteBase
    {
        public R_Class() : base("R") { }
    }
    /// <summary>
    /// リンクリレー デバイスへ、値の取得、設定を行います
    /// </summary>
    public class B_Class : DataReadWriteBase
    {
        public B_Class() : base("B") { }
    }
    /// <summary>
    /// 内部補助リレー デバイスへ、値の取得、設定を行います
    /// </summary>
    public class MR_Class : DataReadWriteBase
    {
        public MR_Class() : base("MR") { }
    }
    /// <summary>
    /// ラッチリレー デバイスへ、値の取得、設定を行います
    /// </summary>
    public class LR_Class : DataReadWriteBase
    {
        public LR_Class() : base("LR") { }
    }
    /// <summary>
    /// コントロールリレー デバイスへ、値の取得、設定を行います
    /// </summary>
    public class CR_Class : DataReadWriteBase
    {
        public CR_Class() : base("CR") { }
    }
    /// <summary>
    /// ワークリレー デバイスへ、値の取得、設定を行います
    /// </summary>
    public class VB_Class : DataReadWriteBase
    {
        public VB_Class() : base("VB") { }
    }
    /// <summary>
    /// 拡張データメモリ デバイスへ、値の取得、設定を行います
    /// </summary>
    public class EM_Class : DataReadWriteBase
    {
        public EM_Class() : base("EM") { }
    }
    /// <summary>
    /// ファイルレジスタ デバイスへ、値の取得、設定を行います
    /// </summary>
    public class FM_Class : DataReadWriteBase
    {
        public FM_Class() : base("FM") { }
    }
    /// <summary>
    /// ファイルレジスタ デバイスへ、値の取得、設定を行います
    /// </summary>
    public class ZF_Class : DataReadWriteBase
    {
        public ZF_Class() : base("ZF") { }
    }
    /// <summary>
    /// リンクレジスタ デバイスへ、値の取得、設定を行います
    /// </summary>
    public class W_Class : DataReadWriteBase
    {
        public W_Class() : base("W") { }
    }
    /// <summary>
    /// テンポラリデータメモリ デバイスへ、値の取得、設定を行います
    /// </summary>
    public class TM_Class : DataReadWriteBase
    {
        public TM_Class() : base("TM") { }
    }
    /// <summary>
    /// インデックスレジスタ デバイスへ、値の取得、設定を行います
    /// </summary>
    public class Z_Class : DataReadWriteBase
    {
        public Z_Class() : base("Z") { }
    }
    /// <summary>
    /// タイマ デバイスへ、値の取得、設定を行います
    /// </summary>
    public class T_Class : DataReadWriteBase
    {
        public T_Class() : base("T") { }
    }
    /// <summary>
    /// タイマ（現在値） デバイスへ、値の取得、設定を行います
    /// </summary>
    public class TC_Class : DataReadWriteBase
    {
        public TC_Class() : base("TC") { }
    }
    /// <summary>
    /// タイマ（設定値） デバイスへ、値の取得、設定を行います
    /// </summary>
    public class TS_Class : DataReadWriteBase
    {
        public TS_Class() : base("TS") { }
    }
    /// <summary>
    /// カウンタ デバイスへ、値の取得、設定を行います
    /// </summary>
    public class C_Class : DataReadWriteBase
    {
        public C_Class() : base("C") { }
    }
    /// <summary>
    /// カウンタ（現在値) デバイスへ、値の取得、設定を行います
    /// </summary>
    public class CC_Class : DataReadWriteBase
    {
        public CC_Class() : base("CC") { }
    }
    /// <summary>
    /// カウンタ（設定値） デバイスへ、値の取得、設定を行います
    /// </summary>
    public class CS_Class : DataReadWriteBase
    {
        public CS_Class() : base("CS") { }
    }
    /// <summary>
    /// デジタルトリマ デバイスへ、値の取得、設定を行います
    /// </summary>
    public class AT_Class : DataReadWriteBase
    {
        public AT_Class() : base("AT") { }
    }
    /// <summary>
    /// コントロールメモリ デバイスへ、値の取得、設定を行います
    /// </summary>
    public class CM_Class : DataReadWriteBase
    {
        public CM_Class() : base("CM") { }
    }
    /// <summary>
    /// ワークメモリ デバイスへ、値の取得、設定を行います
    /// </summary>
    public class VM_Class : DataReadWriteBase
    {
        public VM_Class() : base("VM") { }
    }

    #endregion

    // ST  "ST B2000"           -> ON

    // STS "STS B2000 [1 - 16]" -> ON

    // RS  "RS B2000"           -> OFF

    // RSS "RSS B2000 [1 - 16]" -> OFF
    public abstract class SwitchDeviceBase
    {
        private Func<string, string> SendMethod;
        private string _header;

        public SwitchDeviceBase(string header)
        {
            this._header = header;
        }

        public void SetFunction(Func<string, string> funcPtr)
        {
            this.SendMethod = funcPtr;
        }

        public Switch this[int deviceNo]
        {
            set
            {
                switch (value)
                {
                    case Switch.ON:
                        string mes = String.Concat("ST ", this._header, deviceNo.ToString());
                        string res = this.SendMethod(mes);
                        break;
                    case Switch.OFF:
                        mes = String.Concat("RS ", this._header, deviceNo.ToString());
                        res = this.SendMethod(mes);
                        break;
                    default:
                        break;
                }


            }
        }

        public Switch this[int deviceNo, int num]
        {
            set
            {
                if (num > 16)
                {
                    return;
                }
                switch (value)
                {
                    case Switch.ON:
                        string mes = String.Concat("STS ", this._header, deviceNo.ToString(), " ", num.ToString());
                        string res = this.SendMethod(mes);
                        break;
                    case Switch.OFF:
                        mes = String.Concat("RSS ", this._header, deviceNo.ToString(), " ", num.ToString());
                        res = this.SendMethod(mes);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /// <summary>
    /// 上位リンク通信使用クラス
    /// </summary>
    public class UpperLayerLinkConnection : IDisposable
    {
        #region Const Values

        /// <summary>
        ///　
        /// </summary>
        public const string OK = "OK";
                
        /// <summary>
        /// PLCの動作モード
        /// </summary>
        public enum Mode
        {
            PROGRAM = 0,
            RUN = 1
        }

        /// <summary>
        /// エラー対象表
        /// </summary>
        private static readonly Dictionary<string, string> ErrTable = new Dictionary<string, string>()
        {
            { "000", "エラー発生無し" },
            { "010", "サブルーチン ネスト制限オーバー" },
            { "011", "FOR-NEXT ネスト制限オーバー" },
            { "015", "コンバートエラー" },
            { "022", "マクロ ネスト制限オーバー" },
            { "027", "不正なオブジェクト" },
            { "029", "ラダースタックエラー" },
            { "030", "スキャンタイムオーバー" },
            { "031", "ラダースタックオーバーフロー" },
            { "040", "ラダープログラムなし" },
            { "050", "ユニット設定チェックサム異常" },
            { "051", "ユニット設定情報未設定" },
            { "052", "ユニット未接続" },
            { "053", "拡張バス ユニットタイムアウト" },
            { "054", "ユニット台数 不一致" },
            { "055", "ユニット種別 不一致" },
            { "056", "拡張バス通信エラー" },
            { "057", "エンドユニット未接続" },
            { "059", "ユニットバージョンエラー" },
            { "063", "電源再投入が必要" },
            { "080", "FLASH ROM異常" },
            { "083", "電池電圧の低下" },
            { "085", "時計データ消失" },
            { "086", "RTC異常" },
            { "087", "メモリカード異常" },
            { "088", "メモリカードが抜かれました" },
            { "089", "ストレージアクセス中電源OFF" },
            { "090", "ストレージアクセス完了待ち" },
            { "092", "アクセスウィンドウ異常" },
            { "095", "ファイルアクセス中" },
            { "102", "POWER OFFエラー" },
            { "103", "電断処理超過" },
            { "106", "リセット異常" },
            { "107", "システムエラー" },
            { "118", "不正な伝送データ" },
            { "120", "ウォッチドッグタイマ" },
            { "122", "システムエラー" },
            { "125", "エラーバッファオーバーフロー" },
            { "127", "オートローディング失敗" },
            { "128", "ラダー演算エラー" },
            { "129", "ユニット エラー" },
            { "131", "FLASH ROMフォーマット" },
            { "132", "ラダーファイル不正" },
            { "134", "オートロードフォルダ" },
            { "135", "メモリカードカバーオープン" },
            { "136", "メモリカードが LOCKされています" },
            { "137", "非対応のメモリカード種類" },
            { "141", "CPUメモリ 書込頻度オーバー" },
            { "142", "CPUメモリ 累積書込回数警告" },
            { "143", "CPUメモリ 累積書込回数超過" }
        };

        //private static readonly Dictionary<int, string> ErrTable = new Dictionary<int, string>()
        //{
        //    { 0, "エラー発生無し" },
        //    { 10, "サブルーチン ネスト制限オーバー" },
        //    { 11, "FOR-NEXT ネスト制限オーバー" },
        //    { 15, "コンバートエラー" },
        //    { 22, "マクロ ネスト制限オーバー" },
        //    { 27, "不正なオブジェクト" },
        //    { 29, "ラダースタックエラー" },
        //    { 30, "スキャンタイムオーバー" },
        //    { 31, "ラダースタックオーバーフロー" },
        //    { 40, "ラダープログラムなし" },
        //    { 50, "ユニット設定チェックサム異常" },
        //    { 51, "ユニット設定情報未設定" },
        //    { 52, "ユニット未接続" },
        //    { 53, "拡張バス ユニットタイムアウト" },
        //    { 54, "ユニット台数 不一致" },
        //    { 55, "ユニット種別 不一致" },
        //    { 56, "拡張バス通信エラー" },
        //    { 57, "エンドユニット未接続" },
        //    { 59, "ユニットバージョンエラー" },
        //    { 63, "電源再投入が必要" },
        //    { 80, "FLASH ROM異常" },
        //    { 83, "電池電圧の低下" },
        //    { 85, "時計データ消失" },
        //    { 86, "RTC異常" },
        //    { 87, "メモリカード異常" },
        //    { 88, "メモリカードが抜かれました" },
        //    { 89, "ストレージアクセス中電源OFF" },
        //    { 90, "ストレージアクセス完了待ち" },
        //    { 92, "アクセスウィンドウ異常" },
        //    { 95, "ファイルアクセス中" },
        //    { 102, "POWER OFFエラー" },
        //    { 103, "電断処理超過" },
        //    { 106, "リセット異常" },
        //    { 107, "システムエラー" },
        //    { 118, "不正な伝送データ" },
        //    { 120, "ウォッチドッグタイマ" },
        //    { 122, "システムエラー" },
        //    { 125, "エラーバッファオーバーフロー" },
        //    { 127, "オートローディング失敗" },
        //    { 128, "ラダー演算エラー" },
        //    { 129, "ユニット エラー" },
        //    { 131, "FLASH ROMフォーマット" },
        //    { 132, "ラダーファイル不正" },
        //    { 134, "オートロードフォルダ" },
        //    { 135, "メモリカードカバーオープン" },
        //    { 136, "メモリカードが LOCKされています" },
        //    { 137, "非対応のメモリカード種類" },
        //    { 141, "CPUメモリ 書込頻度オーバー" },
        //    { 142, "CPUメモリ 累積書込回数警告" },
        //    { 143, "CPUメモリ 累積書込回数超過" }
        //};

        private static readonly Dictionary<int, string> Header = new Dictionary<int, string>()
        {

        };
        /// <summary>
        /// PLC通信失敗時の返信ErrorCode
        /// </summary>
        private static readonly Dictionary<string, string> ErrCodeTable = new Dictionary<string, string>()
        {
            { "E0", "デバイス番号異常" } ,
            { "E1", "コマンド異常" } ,
            { "E2", "プログラム未登録" } ,
            { "E4", "書き込み禁止" } ,
            { "E5", "本体エラー" } ,
            { "E6", "コメントなし" }
        };


        #endregion

        #region Property

        private IUpperLinkConnector _Connector;
        /// <summary>
        /// PLCと接続するコネクターを指定します
        /// </summary>
        public IUpperLinkConnector Connector
        {
            get { return this._Connector; }
            set { this._Connector = value; }
        }

        private AfterErrFunc _AfterErrFunc = new AfterErrFunc(
            (string errCode) =>
            {
                throw new Exception();
            });
        /// <summary>
        /// PLC通信異常発生時に呼び出されるコールバック関数を設定します
        /// 初期では例外をthrowします
        /// </summary>
        public AfterErrFunc AfterErrFunc
        {
            set => this._AfterErrFunc = value;
        }

        // M[mode]
        /// <summary>
        /// PLCの動作モードを切り替えます
        /// </summary>
        /// <param name="mode">設定する動作モード</param>
        /// <returns></returns>
        private void ModeChange(Mode mode)
        {
            string command = String.Concat("M", mode.ToString());              // M[mode]\r
            string res = this._Connector.Send(command).Split('\r')[0];     // responce は　....\r\nで返ってくるので、\r以前を取り出す      

            CheckErrorCode(res);
        }

        // ?M
        /// <summary>
        /// PLCの動作モードを取得します
        /// </summary>
        /// <returns></returns>
        private Mode AskMode()
        {
            string res = this._Connector.Send("?M").Split('\r')[0];      // responce は　....\r\nで返ってくるので、\r以前を取り出す      

            if (CheckErrorCode(res))
            {
                return (Mode)Enum.ToObject(typeof(Mode), int.Parse(res));
            }
            else
            {
                return default(Mode);
            }
        }
        /// <summary>
        /// PLCの動作モードを取得、設定します
        /// </summary>
        public Mode PLCMode
        {
            get
            {
                return this.AskMode();
            }
            set
            {
                this.ModeChange(value);
            }
        }
        
        private DM_Class _DM;
        /// <summary>
        /// データメモリ デバイスへ、値の取得、設定を行います
        /// </summary>
        public DM_Class DM { get => _DM; set => _DM = value; }

        private R_Class _R;
        /// <summary>
        /// リレー デバイスへ、値の取得、設定を行います
        /// </summary>
        public R_Class R { get => _R; set => _R = value; }

        private B_Class _B;
        /// <summary>
        /// リンクリレー デバイスへ、値の取得、設定を行います
        /// </summary>
        public B_Class B { get => _B; set => _B = value; }

        private MR_Class _MR;
        /// <summary>
        /// 内部補助リレー デバイスへ、値の取得、設定を行います
        /// </summary>
        public MR_Class MR { get => _MR; set => _MR = value; }

        private LR_Class _LR;
        /// <summary>
        /// ラッチリレー デバイスへ、値の取得、設定を行います
        /// </summary>
        public LR_Class LR { get => _LR; set => _LR = value; }

        private CR_Class _CR;
        /// <summary>
        /// コントロールリレー デバイスへ、値の取得、設定を行います
        /// </summary>
        public CR_Class CR { get => _CR; set => _CR = value; }

        private VB_Class _VB;
        /// <summary>
        /// ワークリレー デバイスへ、値の取得、設定を行います
        /// </summary>
        public VB_Class VB { get => _VB; set => _VB = value; }

        private EM_Class _EM;
        /// <summary>
        /// 拡張データメモリ デバイスへ、値の取得、設定を行います
        /// </summary>
        public EM_Class EM { get => _EM; set => _EM = value; }

        private FM_Class _FM;
        /// <summary>
        /// ファイルレジスタ デバイスへ、値の取得、設定を行います
        /// </summary>
        public FM_Class FM { get => _FM; set => _FM = value; }

        private ZF_Class _ZF;
        /// <summary>
        /// ファイルレジスタ デバイスへ、値の取得、設定を行います
        /// </summary>
        public ZF_Class ZF { get => _ZF; set => _ZF = value; }

        private W_Class _W;
        /// <summary>
        /// リンクレジスタ デバイスへ、値の取得、設定を行います
        /// </summary>
        public W_Class W { get => _W; set => _W = value; }

        private TM_Class _TM;
        /// <summary>
        /// テンポラリデータメモリ デバイスへ、値の取得、設定を行います
        /// </summary>
        public TM_Class TM { get => _TM; set => _TM = value; }

        private Z_Class _Z;
        /// <summary>
        /// インデックスレジスタ デバイスへ、値の取得、設定を行います
        /// </summary>
        public Z_Class Z { get => _Z; set => _Z = value; }

        private T_Class _T;
        /// <summary>
        /// タイマ デバイスへ、値の取得、設定を行います
        /// </summary>
        public T_Class T { get => _T; set => _T = value; }

        private TC_Class _TC;
        /// <summary>
        /// タイマ（現在値） デバイスへ、値の取得、設定を行います
        /// </summary>
        public TC_Class TC { get => _TC; set => _TC = value; }

        private TS_Class _TS;
        /// <summary>
        /// タイマ（設定値） デバイスへ、値の取得、設定を行います
        /// </summary>
        public TS_Class TS { get => _TS; set => _TS = value; }

        private C_Class _C;
        /// <summary>
        /// カウンタ デバイスへ、値の取得、設定を行います
        /// </summary>
        public C_Class C { get => _C; set => _C = value; }

        private CC_Class _CC;
        /// <summary>
        /// カウンタ（現在値) デバイスへ、値の取得、設定を行います
        /// </summary>
        public CC_Class CC { get => _CC; set => _CC = value; }

        private CS_Class _CS;
        /// <summary>
        /// カウンタ（設定値） デバイスへ、値の取得、設定を行います
        /// </summary>
        public CS_Class CS { get => _CS; set => _CS = value; }

        private AT_Class _AT;
        /// <summary>
        /// デジタルトリマ デバイスへ、値の取得、設定を行います
        /// </summary>
        public AT_Class AT { get => _AT; set => _AT = value; }

        private CM_Class _CM;
        /// <summary>
        /// コントロールメモリ デバイスへ、値の取得、設定を行います
        /// </summary>
        public CM_Class CM { get => _CM; set => _CM = value; }

        private VM_Class _VM;
        /// <summary>
        /// ワークメモリ デバイスへ、値の取得、設定を行います
        /// </summary>
        public VM_Class VM { get => _VM; set => _VM = value; }


        #endregion

        /// <summary>
        /// connectorによって通信を行うように初期化します
        /// </summary>
        /// <param name="connector">通信クラス</param>
        public UpperLayerLinkConnection(IUpperLinkConnector connector)
        {
            this.Init(connector);
        }

        /// <summary>
        /// connectorによって通信を行うように初期化します. connectorは 設定fileによって再初期化されます
        /// </summary>
        /// <param name="connector">通信クラス</param>
        /// <param name="file">設定ファイル</param>
        public UpperLayerLinkConnection(IUpperLinkConnector connector, string file)
        {
            connector.Init(file);

            this.Init(connector);
        }

        #region Public Method

        /// <summary>
        /// 通信パラメータの初期化処理
        /// </summary>
        /// <param name="connector"></param>
        public void Init(IUpperLinkConnector connector)
        {
            // 通信エンコーディングはASCII固定
            connector.Encoding = Encoding.ASCII;
            
            this._Connector = connector;
            connector.Connect();
            var sendFunc = new Func<string, string>(this.SendData);

            #region RD,RDS,WR,WRSコマンド用の TemplateClass 初期化

            this._DM    = new DM_Class();
            this._R     = new R_Class();
            this._B     = new B_Class();
            this._MR    = new MR_Class();
            this._LR    = new LR_Class();
            this._CR    = new CR_Class();
            this._VB    = new VB_Class();
            this._EM    = new EM_Class();
            this._FM    = new FM_Class();
            this._ZF    = new ZF_Class();
            this._W     = new W_Class();
            this._TM    = new TM_Class();
            this._Z     = new Z_Class();
            this._T     = new T_Class();
            this._TC    = new TC_Class();
            this._TS    = new TS_Class();
            this._C     = new C_Class();
            this._CC    = new CC_Class();
            this._CS    = new CS_Class();
            this._AT    = new AT_Class();
            this._CM    = new CM_Class();
            this._VM    = new VM_Class();

            this._DM.SetFunction(sendFunc);
            this._R.SetFunction(sendFunc);
            this._B.SetFunction(sendFunc);
            this._MR.SetFunction(sendFunc);
            this._LR.SetFunction(sendFunc);
            this._CR.SetFunction(sendFunc);
            this._VB.SetFunction(sendFunc);
            this._EM.SetFunction(sendFunc);
            this._FM.SetFunction(sendFunc);
            this._ZF.SetFunction(sendFunc);
            this._W.SetFunction(sendFunc);
            this._TM.SetFunction(sendFunc);
            this._Z.SetFunction(sendFunc);
            this._T.SetFunction(sendFunc);
            this._TC.SetFunction(sendFunc);
            this._TS.SetFunction(sendFunc);
            this._C.SetFunction(sendFunc);
            this._CC.SetFunction(sendFunc);
            this._CS.SetFunction(sendFunc);
            this._AT.SetFunction(sendFunc);
            this._CM.SetFunction(sendFunc);
            this._VM.SetFunction(sendFunc);

            #endregion

        }

        /// <summary>
        /// 通信切断処理
        /// </summary>
        public void DisConnect()
        {
            if (this._Connector.IsConnect)
            {
                this._Connector.DisConnect();
            }
        }

        /// <summary>
        /// PLCで発生しているエラーをクリアします
        /// </summary>
        /// <returns></returns>
        public void ErrorClear()
        {
            string res = this._Connector.Send("ER").Split('\r')[0];         // responce は　....\r\nで返ってくるので、\r以前を取り出す

            CheckErrorCode(res);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mes"></param>
        /// <returns></returns>
        private string SendData(string mes)
        {
            var res = this._Connector.Send(mes);
            if (CheckErrorCode(res))
            {
                return res;
            }
            else
            {
                return null;
            }
        }

        // ?E
        /// <summary>
        /// エラーコードに対応したメッセージを取得する
        /// </summary>
        /// <returns></returns>
        public string GetPLCErr()
        {
            string res = this._Connector.Send("?E");
            if (!CheckErrorCode(res))
            {
                string mes = String.Format("ErrorCode: {0}\r\n{1}", res, ErrTable[res]);
                return mes;
            }
            else
            {
                return res;
            }
        }        

        #region 必要なし (RD,WR系コマンドと同様)

        // ST  "ST B2000"           -> ON

        // STS "STS B2000 [1 - 16]" -> ON

        // RS  "RS B2000"           -> OFF

        // RSS "RSS B2000 [1 - 16]" -> OFF

        #endregion

        // ?K 機種の問い合わせ

        // WRT 時刻設定
        /// <summary>
        /// PLCへ時刻を設定します
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public string SetDateTime(DateTime time)
        {
            string cmd = String.Format(
                "WRT {0:00} {1:00} {2:00} {3:00} {4:00} {5:00} {6}",
                (time.Year - 2000),            // 年  ：データ１ -> 2000 を 00 とする
                time.Month,                    // 月  ：データ２
                time.Day,                      // 日  ：データ３
                time.Hour,                     // 時間：データ４
                time.Minute,                   // 分  ：データ５
                time.Second,                   // 秒  ：データ６
                (int)time.DayOfWeek            // 曜日：データ７
                );

            string res = this._Connector.Send(cmd);
            if (!CheckErrorCode(res))
            {
                string mes = String.Format("ErrorCode: {0}\r\n{1}", res, ErrTable[res]);
                return mes;
            }
            else
            {
                return res;
            }
        }

        // RDE 連続データ読み出し

        // WRE 連続データ書き込み

        // WS 設定値書き込み

        // WSS 連続設定値書き込み

        // MBS モニタ登録

        // MWS

        // MBR

        // MWR

        // RDC コメント読み出し

        // BE バンク切り換え

        // URD 拡張ユニットバッファメモリ読み出し

        // UWR 拡張ユニットバッファメモリ書き込み

        #endregion


        #region Private Method

        /// <summary>
        /// 応答がエラーコードならメッセージへ変換して登録してあるエラーコールバック関数を呼び出す、OKならtrueを返す
        /// </summary>
        /// <param name="responce">PLCからの応答メッセージ</param>
        /// <returns></returns>
        private bool CheckErrorCode(string responce)
        {
            var re = new Regex(@"E\d");
            if (re.IsMatch(responce))
            {
                responce = responce.Split('\r')[0];
                if (this._AfterErrFunc != null)
                {
                    string mes = String.Format("ErrorCode: {0}\r\n{1}", responce, ErrCodeTable[responce]);
                    this._AfterErrFunc(mes);
                }

                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion


        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージ リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~UpperLayerLinkConnection() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
        
    }

}
