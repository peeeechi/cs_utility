﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace Utility
{
    public enum LogLevel
    {
        /// <summary>
        /// 
        /// </summary>
        Debug = 0,
        /// <summary>
        /// 
        /// </summary>
        Info,
        /// <summary>
        /// 
        /// </summary>
        Error,
        /// <summary>
        /// 
        /// </summary>
        Fatal

    }

    [Serializable]
    /// <summary>
    /// 
    /// </summary>
    public class Logger
    {
        private enum FileEX
        {
            csv = 0,
            json,
            log
        }

        private string _encoding = "utf-8";
        /// <summary>
        /// Log書き込みに使用するエンコード
        /// </summary>
        public Encoding encoding
        {
            get => Encoding.GetEncoding(this._encoding);
            set => this._encoding = value.BodyName;
        }

        /// <summary>
        /// メッセージフォーマット
        /// </summary>
        private string _mesFormat;
        private LogLevel _LogLevel = LogLevel.Info;
        private FileEX _fileEX;

        public LogLevel LogLevel
        {
            get => this._LogLevel;
            set => this._LogLevel = value;
        }

        private const string DateFormat = "yyyy_MM_dd";
        private const string DateFormat_In_Log = "yyyy/MM/dd HH:mm:ss";
        private const string CLogFileName = "{0}Log.csv";

        private string _LogFileName;
        //private System.Diagnostics.DefaultTraceListener listener;
        //private System.Diagnostics.TextWriterTraceListener listener;

        public Logger()
        {
            this._LogFileName = String.Format(CLogFileName, DateTime.Now.ToString(DateFormat));
            Init();
        }

        public Logger(string LogFileName)
        {
            this._LogFileName = LogFileName;
            Init();
        }

        public Logger(string LogFileName, Encoding encoding)
        {
            this._LogFileName = LogFileName;
            this.encoding = encoding;
            Init();
        }

        ~Logger()
        {
            if (this._fileEX == FileEX.json)
            {
                //listener.WriteLine("]");
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(this._LogFileName, true, this.encoding))
                {
                    sw.WriteLine("]");
                }
            }

        }

        private void Init()
        {
            string ex = System.IO.Path.GetExtension(this._LogFileName).ToLower().Split('.')[1];

            if (ex == "csv")        // CSV 形式の場合
            {
                this._mesFormat = "{0},{1},{2}_{3},{4},{5}";
                this._fileEX = FileEX.csv;

                if (System.IO.File.Exists(this._LogFileName))
                {
                    string text;
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(this._LogFileName, this.encoding))
                    {
                        text = sr.ReadToEnd();
                    }

                    if (text == "")
                    {
                        using (System.IO.StreamWriter sw = new System.IO.StreamWriter(this._LogFileName, true, this.encoding))
                        {
                            sw.WriteLine("Date,LogLevel,Program,Line,Message");
                        }
                    }

                }
                else
                {
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(this._LogFileName, true, this.encoding))
                    {
                        sw.WriteLine("Date,LogLevel,Program,Line,Message");
                    }
                }
            }
            else if (ex == "json")  // JSON 形式の場合
            {
                string baseFormat = "\"Date\":\"{0}\",\"LogLevel\":\"{1}\",\"Program\":\"{2}_{3}\",\"Line\":{4},\"Message\":\"{5}\"";
                this._mesFormat = "{{" + baseFormat + "}},";
                this._fileEX = FileEX.json;

                if (System.IO.File.Exists(this._LogFileName))
                {
                    string text;
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(this._LogFileName, this.encoding))
                    {
                        text = sr.ReadToEnd();
                    }

                    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"[\[\]]");
                    var after = regex.Replace(text, "");

                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(this._LogFileName, true, this.encoding))
                    {
                        var buf = String.Concat("[", after);
                        sw.WriteLine(buf);
                    }
                }
                else
                {
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(this._LogFileName, true, this.encoding))
                    {
                        var buf = String.Concat("[");
                        sw.WriteLine(buf);
                    }
                }
            }
            else                    // LOG 形式の場合
            {
                this._mesFormat = "[Date] {0}, [LogLevel] {1}, [Program] {2}_{3}, [Line] {4}, [Message] {5}";
                this._fileEX = FileEX.log;

            }

            //this.listener = (System.Diagnostics.DefaultTraceListener)System.Diagnostics.Trace.Listeners["Default"];
            //this.listener.LogFileName   = this._LogFileName;            
        }

        private string MakeLogMessage(string mes, LogLevel level)
        {

            System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace(true);

            string LogMsg = String.Format(this._mesFormat
                , System.DateTime.Now.ToString(DateFormat_In_Log)
                , Enum.GetName(typeof(LogLevel), level)
                , stackTrace.GetFrame(1).GetMethod().ReflectedType.FullName
                , stackTrace.GetFrame(1).GetMethod().Name
                , stackTrace.GetFrame(1).GetFileLineNumber()
                , mes
                );

            return LogMsg;
        }

        
        [MethodImpl(MethodImplOptions.Synchronized)]
        private void TraceWrite(string mes, LogLevel level)
        {
            if (this._LogLevel <= level)
            {
                string logMsg = MakeLogMessage(mes, level);
                //listener.WriteLine(logMsg);
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(this._LogFileName, true, this.encoding))
                {
                    sw.WriteLine(logMsg);
                }
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void DebugWrite(string mes, LogLevel level)
        {
#if Debug
            if (this._LogLevel <= level)
            {
                string logMsg = MakeLogMessage(mes, level);
                //System.Diagnostics.Debug.WriteLine(logMsg);
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(this._LogFileName, true, this.encoding))
                {
                    sw.WriteLine(logMsg);
                }
            }
#endif
        }

        public void Debug(string mes)
        {
            this.TraceWrite(mes, LogLevel.Debug);
        }

        public void Info(string mes)
        {
            this.TraceWrite(mes, LogLevel.Info);
        }

        public void Error(string mes)
        {
            this.TraceWrite(mes, LogLevel.Error);
        }

        public void Fatal(string mes)
        {
            this.TraceWrite(mes, LogLevel.Fatal);
        }
    }
}
