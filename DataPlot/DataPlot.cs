﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.DataPlot
{
    public class DataPlot
    {
        private List<Utility.DataPlot.Series> GrafList = new List<Utility.DataPlot.Series>();

        public void Plot(Utility.DataPlot.Series series)
        {
            this.GrafList.Add(series);
        }
        public void Plot(List<Utility.DataPlot.Series> series)
        {
            this.GrafList.AddRange(series);
        }
        public void Plot(
            List<double> Y,
            string grafName = null,
            System.Windows.Forms.DataVisualization.Charting.SeriesChartType type = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point)
        {
            Utility.DataPlot.Series series = new Utility.DataPlot.Series(Y);
            series.GrafTitle = grafName;
            series.Type = type;
            this.GrafList.Add(series);
        }

        public void Plot(
            List<double> Y,
            System.Drawing.Color color,
            string grafName = null,
            System.Windows.Forms.DataVisualization.Charting.SeriesChartType type = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point)
        {
            Utility.DataPlot.Series series = new Utility.DataPlot.Series(Y);
            series.Color = color;
            series.GrafTitle = grafName;
            series.Type = type;
            this.GrafList.Add(series);
        }



        public void Plot(List<double> X, List<double> Y, string grafName = null, System.Windows.Forms.DataVisualization.Charting.SeriesChartType type = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point)
        {
            Utility.DataPlot.Series series = new Utility.DataPlot.Series(X, Y);
            series.GrafTitle = grafName;
            series.Type = type;
            this.GrafList.Add(series);
        }

        public void Plot(List<double> X, List<double> Y, System.Drawing.Color color, string grafName = null, System.Windows.Forms.DataVisualization.Charting.SeriesChartType type = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point)
        {
            Utility.DataPlot.Series series = new Utility.DataPlot.Series(X, Y);
            series.Color = color;
            series.GrafTitle = grafName;
            series.Type = type;
            this.GrafList.Add(series);
        }


        public void Show()
        {
            GrafForm graf = new GrafForm(this.GrafList);
            graf.ShowDialog();
        }


    }
}
