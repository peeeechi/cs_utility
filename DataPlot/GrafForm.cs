﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Utility.DataPlot
{
    public partial class GrafForm : Form
    {


        private string _Title;
        private List<Series> GrafDatas = new List<Series>();

        public GrafForm(Series series)
        {
            InitializeComponent();
            this.GrafDatas.Add(series);
        }
        public GrafForm(List<Series> series)
        {
            InitializeComponent();
            this.GrafDatas.AddRange(series);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Show();
            //PlotSinCos();
            //PlotSinCos2();
        }

        private void Show()
        {
            
            chart1.Series.Clear();

            for (int i = 0; i < this.GrafDatas.Count; i++)
            {
                // 1.Seriesの追加
                var title = this.GrafDatas[i].GrafTitle != null ? this.GrafDatas[i].GrafTitle : $"Graf{i}";
                chart1.Series.Add(title);

                // 2.グラフのタイプの設定
                chart1.Series[title].ChartType = GrafDatas[i].Type;

                if (!this.GrafDatas[i].Color.IsEmpty)
                {
                    chart1.Series[title].Color = GrafDatas[i].Color;
                }


                // 3.座標の入力
                for (int idx = 0; idx < this.GrafDatas[i].X.Count; idx++)
                {
                    chart1.Series[title].Points.AddXY(this.GrafDatas[i].X[idx], this.GrafDatas[i].Y[idx]);
                }                

            }
        }

        //written by whoopsidaisies
        private void PlotSinCos()
        {
            // 1.Seriesの追加
            chart1.Series.Clear();
            chart1.Series.Add("sin");
            chart1.Series.Add("cos");
            chart1.Series.Add("mine");

           

            // 2.グラフのタイプの設定
            chart1.Series["sin"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart1.Series["cos"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart1.Series["mine"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            chart1.Series["mine"].Color = System.Drawing.Color.Aqua;

            // 3.座標の入力
            for (double theta = 0.0; theta <= 2 * Math.PI; theta += Math.PI / 360)
            {
                chart1.Series["sin"].Points.AddXY(theta, Math.Sin(theta));
                chart1.Series["cos"].Points.AddXY(theta, Math.Cos(theta));
                chart1.Series["mine"].Points.AddXY(theta, Math.Cos(theta));
            }
        }

        //written by whoopsidaisies
        private void PlotSinCos2()
        {
            // 1.Seriesの追加
            chart1.Series.Clear();
            chart1.Series.Add("test");

            // 2.グラフのタイプの設定
            chart1.Series["test"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

            // 3.座標の入力
            for (double theta = 0.0; theta <= 2 * Math.PI; theta += Math.PI / 360)
            {
                chart1.Series["test"].Points.AddXY(Keisan(2, theta, 0), Keisan(2, theta, 1)); //書き換え
            }
        }

        //関数を追加
        private double Keisan(double n, double t, int selectXY)
        {
            double x, y;

            x = n * Math.Cos(t) + Math.Cos(n * t);
            y = n * Math.Sin(t) + Math.Sin(n * t);

            return (selectXY == 0) ? x : y;
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }
    }

    public class Series
    {
        public string GrafTitle;

        public List<double> X = new List<double>();
        public List<double> Y = new List<double>();
        public System.Drawing.Color Color = default(System.Drawing.Color);
        public System.Windows.Forms.DataVisualization.Charting.SeriesChartType Type;
        public Series()
        {

        }

        public Series(List<double> Y, System.Windows.Forms.DataVisualization.Charting.SeriesChartType type = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line)
        {
            this.X = Enumerable.Range(0, Y.Count).ToList().ConvertAll(x => (double)x);
            this.Y = Y;
            this.Type = type;
        }
        public Series(List<double> X, List<double> Y, System.Windows.Forms.DataVisualization.Charting.SeriesChartType type = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line)
        {
            this.X = X;
            this.Y = Y;
            this.Type = type;
        }
    }
}
