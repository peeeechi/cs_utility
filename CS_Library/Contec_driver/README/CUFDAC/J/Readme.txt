=====================================================================
=         USB対応入出力モジュールのファームウェアについて           =
=                                     CUFDAC.bin Ver1.18            =
=                                                  CONTEC Co.,Ltd.  =
=====================================================================

◆ 目次
=======
  はじめに
  ファームウェアファイル
  適用されるモジュール
  注意事項
  バージョンアップ履歴


◆ はじめに
===========
  ここではUSB対応入出力モジュールのファームウェアについて説明をしています。


◆ファームウェアファイル
========================
  ファームウェアファイルはバイナリ形式で、USBモジュールにダウンロードして使用します。
  API-USBP(WDM)には最新のファームウェアファイルが格納されています。
  お使いのUSBモジュールのファームウェアが旧バージョンの場合、ダウンロードによる
  バージョンアップを実施してください。
  バージョンの確認、ダウンロード方法についてはUSBモジュールの解説書を参照願います。


◆適用されるモジュール
======================
  以下のUSBモジュールに対応しています。
    ・デジタル入出力モジュール
      DIO-8/8(USB)
      DI-16(USB)
      DO-16(USB)
      DIO-16/16(USB)
      DI-32(USB)
      DO-32(USB)
    ・アナログ入出力モジュール
      ADI12-8(USB)
      DAI12-4(USB)
      ADI16-4(USB)
      DAI16-4(USB)
    ・カウンタ入力モジュール
      CNT24-2(USB)
    ・温度センサ入力モジュール
      PTI-4(USB)


◆注意事項
==========
  ・CPU-CA10(USB)用のファームウェアではありません。CPU-CA10(USB)には、別のファームウェアが搭載されます。


◆バージョンアップ履歴
=======================

  Ver1.15->Ver1.18 (Web提供 2015.12)
  ----------------------------------------
  アナログ入出力モジュール
  ・DAI16-4(USB)GYまたはDAI12-4(USB)GYで、AioSetAoSamplingClock() によるクロック設定が正常に行えない場合がある不具合を修正。
  ・DAI16-4(USB)GY + 増設モジュールの構成で、AOサンプリングを増設モジュール側まで使用するチャネル数に設定すると、クロックが約1%遅く設定される不具合を修正
  カウンタ入力モジュール
  ・CNT24-2(USB)GY + 増設モジュールの構成で、CntPreset実行後、カウントスタートしても増設モジュール側にプリセット値が反映されていない不具合を修正

  Ver1.14->Ver1.15 (API-USBP(WDM) Ver4.20)
  ----------------------------------------
  アナログ入出力モジュール
  ・DAI16-4(USB)GYまたはDAI12-4(USB)GYで、AioStartAoの動作を途中で停止してその後AioSingleAoを実行すると
    正しい電圧が出力されない不具合を修正

  Ver1.13->Ver1.14 (API-USBP(WDM) Ver3.30)
  ----------------------------------------
  温度センサ入力モジュール
  ・PTI-4(USB)にPTI-4(FIT)を増設した場合、増設モジュールにアクセスできない不具合修正

  Ver1.12->Ver1.13 (API-USBP(WDM) Ver3.10)
  ----------------------------------------
  アナログ入出力モジュール
  ・DAI16-4(USB)において、Ring転送で転送回数を有限にすると２回目以降の転送時に先頭の数データが出力されない不具合を修正
  ・DAI16-4(USB)において、セットするデータ数が少ない時に、Ringで有限転送を設定して複数回転送を行おうとすると
    転送されるデータ数が減っていく不具合を修正
  ・DAI16-4(USB)において、データ数が少ない時(33以下)にRing転送で１回分のデータしか出力されない不具合を修正

  Ver1.10->Ver1.12 (Web提供 2005.07.04)
  ----------------------------------------
  アナログ入出力モジュール
  ・ADI12-8(USB)GYにおいて、複数チャネル使用時に計測できない不具合修正
  ・アナログ出力モジュールにおいて、サンプリングクロックエラー発生後に動作が異常となる不具合修正

  Ver1.04->Ver1.10 (API-USBP(WDM) Ver3.00)
  ----------------------------------------
  デジタル入出力モジュール
  ・対応モジュール追加
    DIO-16/16(USB), DI-32(USB), DO-32(USB)
  アナログ入出力モジュール
  ・対応モジュール追加
    ADI16-4(USB), DAI16-4(USB)
  ・計測データが256Kデータ以上でリピート動作が異常となる不具合修正
  温度センサ入力モジュール
  ・PTI-4(USB)のサポート

  Ver1.03->Ver1.04 (API-USBP(WDM) Ver2.10)
  ----------------------------------------
   アナログ入出力モジュール
  ・API-PAC(W32)のAPI-AIO(WDM)と共存可能にする修正
  ・ホストドライバとのバージョンチェックを追加

  Ver1.02->Ver1.03 (API-USBP(WDM) Ver2.00)
  ----------------------------------------
  デジタル入出力モジュール
  ・DioResetDevice()実行時のリセット動作不具合について修正
  ・デジタルフィルタ関数を追加
  アナログ入出力モジュール
  ・DAI12-4(USB)においてアナログ出力値が異常となる場合がある不具合修正
    （一部のDAI12-4(USB)ハードウェアにおいて発生）
  カウンタ入力モジュール
  ・CntResetDevice()実行時のリセット動作不具合について修正

  Ver1.00->Ver1.02 (API-USBP(WDM) Ver1.10)
  ----------------------------------------
  ・API実行処理時間の改善

以上