=====================================================================
=              Firmware for I/O Module with USB                     =
=                                     CUFDAC.bin Ver1.18            =
=                                                  CONTEC Co.,Ltd.  =
=====================================================================

- Contents
===========
  Introduction
  Firmware file
  Supported module
  Notes
  Career at version up


- Introduction
==============
  Here, the firmware of I/O Module with USB is explained.


- Firmware file
===============
  A firmware file is binary form, and is downloaded and used for a USB module.
  The newest firmware file is stored in API-USBP(WDM).
  If the firmware of USB module is the old version, please update the firmware with update tool.


- Supported module
==================
  The modules which can be used with this firmware is shown below.
      DIO-8/8(USB)
      DI-16(USB)
      DO-16(USB)
      DIO-16/16(USB)
      DI-32(USB)
      DO-32(USB)
      ADI12-8(USB)
      DAI12-4(USB)
      ADI16-4(USB)
      DAI16-4(USB)
      CNT24-2(USB)
      PTI-4(USB)


- Notes
=======
  - This is not a firmware for CPU-CA10(USB).


- Career at version up
======================

  Ver1.15->Ver1.18 (web. 2015.12)
  ----------------------------------------
  For Analog Input/Output Module
  - Fixed bug in DAI16-4 (USB) GY or GY (USB) DAI12-4, AioSetAoSamplingClock() 
    clock settings cannot be successfully.
  - Fix AO sampling is used to the expansion module, composed of DAI16-4 (USB) GY 
    + additional module channel number setting the clock is set late about 1 percent
  For Counter Input Module
  - Fix CNT24-2 (USB) GY + expansion modules of the CntPreset after running the count 
    to start preset value is not on the expansion module

  Ver1.14->Ver1.15 (API-USBP(WDM) Ver4.20)
  ----------------------------------------
  For Analog Input/Output Module
  - Solved the problem that output current is not correct when executing AioSingleAo
    after stopping AioStartAo motion with DAI16-4(USB)GY or DAI12-4(USB)GY.

  Ver1.13->Ver1.14 (API-USBP(WDM) Ver3.30)
  ----------------------------------------
  For Input module for tempareture sensor
  - Fault correction to which increase module is inaccessible when PTI-4(FIT) is increased in PTI-4(USB).

  Ver1.12->Ver1.13 (API-USBP(WDM) Ver3.10)
  ----------------------------------------
  For Analog Input/Output Module
  - In DAI16-4(USB), when the forwarding frequency is made limited by Ring forwarding,
    trouble to which the number of first data is not output when forwarding it
    since the second times is corrected.
  - In DAI16-4(USB), when a limited forwarding is set with Ring when
    the set number of data is little and it forwards it two or more times,
    trouble to which the forwarded number of data decreases is corrected. 
  - In DAI16-4(USB), trouble to which only worth of data is output once by Ring forwarding when
    the number is a data little (33 or less) is corrected. 

  Ver1.10->Ver1.12 (web. 2005.07.04)
  ----------------------------------------
  For Analog Input/Output Module
  - Fault correction that cannot be measured when two or more channels are used in ADI12-8(USB).
  - Fault correction that operation becomes abnormal after sampling clock error 
    occurs in analog output module.

  Ver1.04->Ver1.10 (API-USBP(WDM) Ver3.00)
  ----------------------------------------
  For Digital Input/Output Module
  - Add the usable module
    DIO-16/16(USB), DI-32(USB), DO-32(USB)
  For Analog Input/Output Module
  - Add the usable module
    ADI16-4(USB), DAI16-4(USB)
  - Fault correction of measurement data that repetition operation is made abnormal
    more than 256K data.
  For Input module for tempareture sensor
  - PTI-4(USB) is supported.

  Ver1.03->Ver1.04 (API-USBP(WDM) Ver2.10)
  ----------------------------------------
  For Analog Input/Output Module
  - Correction of the fault which API-USBP(WDM) cannot coexist with API-PAC(W32).
  - Add the function which checks a version.

  Ver1.02->Ver1.03 (API-USBP(WDM) Ver2.00)
  ----------------------------------------
  For Digital Input/Output Module
  - Digital Fixed the reset action fault at the time of DioResetDevice() execution.
  - Added the digital filter functions.
  For Analog Input/Output Module
  - Fault correction to which an analog output value may become unusual in DAI12-4(USB).
  For Counter Input Module
  - Fixed the reset action fault at the time of CntResetDevice() execution.

  Ver1.00->Ver1.02 (API-USBP(WDM) Ver1.10)
  ----------------------------------------
  - An improvement of API execution processing time.
