﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace Utility
{
    /// <summary>
    /// ボーレート
    /// </summary>
    public enum BaudRate
    {
        bps_75      = 75,
        bps_110     = 110,
        bps_300     = 300,
        bps_1200    = 1200,
        bps_2400    = 2400,
        bps_4800    = 4800,
        bps_9600    = 9600,
        bps_19200   = 19200,
        bps_38400   = 38400,
        bps_57600   = 57600,
        bps_115200  = 115200
    }

    /// <summary>
    /// データビット
    /// </summary>
    public enum DataBits
    {
        Bit5 = 5,
        Bit6 = 6,
        Bit7 = 7,
        Bit8 = 8,
        Bit9 = 9
    }

    /// <summary>
    /// シリアル通信クラス
    /// </summary>
    public class SerialConnector
    {
        #region Const

        #endregion

        /// <summary>
        /// シリアル通信の設定ファイルクラス
        /// </summary>
        public class Param
        {
            /// <summary>
            /// COM Port No
            /// </summary>
            public string ComPort = "com1";

            /// <summary>
            /// 通信ボーレート
            /// </summary>
            public BaudRate BaudRate = BaudRate.bps_19200;

            /// <summary>
            /// データビット
            /// </summary>
            public DataBits DataBits = DataBits.Bit8;

            /// <summary>
            /// 通信時の文字列エンコード
            /// </summary>
            public string encoding = "utf-8";

            /// <summary>
            /// 受信時に終端とみなす改行文字
            /// </summary>
            public string Res_EndCode = "\r\n";

            /// <summary>
            /// 送信時に文字列末につける終端文字
            /// </summary>
            public string Send_EndCode = "\r\n";

            /// <summary>
            /// パリティビット
            /// </summary>
            public Parity parity = Parity.Even;

            /// <summary>
            /// ストップビット
            /// </summary>
            public StopBits stopBits = StopBits.One;

            /// <summary>
            /// 読み取りタイムアウト時間 [ミリ秒]
            /// </summary>
            public int ReadTimeout = 3000;

            /// <summary>
            /// 書き込みタイムアウト時間 [ミリ秒]
            /// </summary>
            public int WriteTimeout = 3000;

            /// <summary>
            /// 本体のコピーを返す
            /// </summary>
            /// <returns></returns>
            public Param Clone()
            {
                Param clone = new Param()
                {
                    ComPort         = this.ComPort,
                    BaudRate        = this.BaudRate,
                    DataBits        = this.DataBits,
                    encoding        = this.encoding,
                    Res_EndCode     = this.Res_EndCode,
                    Send_EndCode    = this.Send_EndCode,
                    parity          = this.parity,
                    stopBits        = this.stopBits,
                    ReadTimeout     = this.ReadTimeout,
                    WriteTimeout    = this.WriteTimeout
                };

                return clone;
            }

        }

        #region Property

        private Param _param;
        public Param param
        {
            get
            {
                return this._param.Clone();
            }
        }


        /// <summary>
        /// COM Port No
        /// </summary>
        public string ComPort
        {
            get => this._param.ComPort;
        }

        /// <summary>
        /// 通信ボーレート
        /// </summary>
        public BaudRate BaudRate
        {
            get => this._param.BaudRate;
        }

        /// <summary>
        /// データビット
        /// </summary>
        public DataBits DataBits = DataBits.Bit8;

        /// <summary>
        /// 通信時の文字列エンコード
        /// </summary>
        public string encoding
        {
            get => this._param.encoding;
            set
            {
                this._serialPort.Encoding = Encoding.GetEncoding(value);
                this._param.encoding = value;
            }
        }

        /// <summary>
        /// 受信時に終端とみなす改行文字
        /// </summary>
        public string Res_EndCode
        {
            get => this._param.Res_EndCode;
            set
            {
                if (value == "\r\n" || value == "\r" || value == "\n")
                {
                    this._param.Res_EndCode = value;
                }
            }
        }

        /// <summary>
        /// 送信時に文字列末につける終端文字
        /// </summary>
        public string Send_EndCode
        {
            get { return this._param.Send_EndCode; }
            set
            {
                if (value == "\r\n" || value == "\r" || value == "\n")
                {
                    this._param.Send_EndCode = value;
                }
            }
        }

        /// <summary>
        /// パリティビット
        /// </summary>
        public Parity parity
        {
            get => this._param.parity;
        }

        /// <summary>
        /// ストップビット
        /// </summary>
        public StopBits stopBits
        {
            get => this._param.stopBits;
        }

        /// <summary>
        /// 読み取りタイムアウト時間 [ミリ秒]
        /// </summary>
        public int ReadTimeout
        {
            get => this._param.ReadTimeout;
            set
            {
                var timeOut                     = (value > 0) ? value : System.IO.Ports.SerialPort.InfiniteTimeout;
                this._serialPort.ReadTimeout    = timeOut;
                this._param.ReadTimeout         = timeOut;
            }
        }

        /// <summary>
        /// 書き込みタイムアウト時間 [ミリ秒]
        /// </summary>
        public int WriteTimeout
        {
            get => this._param.WriteTimeout;
            set
            {
                var timeOut                     = (value > 0) ? value : System.IO.Ports.SerialPort.InfiniteTimeout;
                this._serialPort.WriteTimeout   = timeOut;
                this._param.WriteTimeout        = timeOut;
            }
        }

        #endregion

        private SerialPort _serialPort;        

        /// <summary>
        /// 接続可能なSerialPortの一覧を返す
        /// </summary>
        public string[] GetPortNames
        {
            get => System.IO.Ports.SerialPort.GetPortNames();
        }        

        /// <summary>
        /// 接続状態を取得する
        /// </summary>
        public bool IsConnect
        {
            get => this._serialPort.IsOpen;
        }
        
        public SerialConnector(string comport)
        {
            Param param = new Param();
            param.ComPort = comport;
            this.Init(param);

        }

        public void Init(Param param)
        {
            this._serialPort = new SerialPort()
            {
                PortName        = param.ComPort,
                BaudRate        = (int)param.BaudRate,
                DataBits        = (int)param.DataBits,
                Encoding        = Encoding.GetEncoding(param.encoding),
                NewLine         = param.Res_EndCode,
                Parity          = param.parity,
                StopBits        = param.stopBits,
                ReadTimeout     = param.ReadTimeout,
                WriteTimeout    = param.WriteTimeout

            };

            this._param = param;

            //if (this._serialPort == null)
            //{

            //}
        }

        public void Init()
        {
            if (this._param != null)
            {
                Init(this._param);
            }
        }

        public void Connect()
        {
            if (this._serialPort == null)
            {
                Init();
            }

            if (this._serialPort.IsOpen == false)
            {
                this._serialPort.Open();
            }
        }

        public string Send(string mes)
        {
            try
            {
                this._serialPort.Write(String.Concat(mes, this._param.Send_EndCode));
                
                string res = "";
                res = this._serialPort.ReadLine();

                return res;
            }
            catch (Exception err)
            {
                return err.Message;
            }
        }

        public void DisConnect()
        {
            if (this._serialPort != null && this._serialPort.IsOpen == true)
            {
                this._serialPort.Close();
            }
        }


        //public Param SettingLoad(string fileName)
        //{

        //}

        //public void SettingLoad(string fileName)
        //{

        //}
    }

}
