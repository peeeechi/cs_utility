﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;



namespace Utility
{
    /// <summary>
    /// 
    /// </summary>
    public class TCP_IP_Connect
    {
        public static class Const
        {
            public const int DefaultPort = 80;
            public const string LocalHost = "127.0.0.1";
        }

        public class Client
        {
            /// <summary>
            /// パラメータ保持クラス
            /// </summary>
            public class Param
            {
                /// <summary>
                /// 送信先のIPアドレス
                /// </summary>
                public string IP;

                /// <summary>
                /// 送信先のPortNo
                /// </summary>
                public int SendPort;

                /// <summary>
                /// 受信元のPortNO
                /// </summary>
                public int RecivePort;

                /// <summary>
                /// 受信時に終端とみなす改行文字
                /// </summary>
                public string Res_EndCode = "\r\n";

                /// <summary>
                /// 送信時に文字列末につける終端文字
                /// </summary>
                public string Send_EndCode = "\r\n";

                /// <summary>
                /// 送受信用のパラメータ
                /// </summary>
                public string encoding = "utf-8";

                /// <summary>
                /// 送信タイムアウト時間 [ミリ秒]
                /// </summary>
                public int WriteTimeOut = System.Threading.Timeout.Infinite;     //初期値は無限に待機

                /// <summary>
                /// 受信タイムアウト時間 [ミリ秒]
                /// </summary>
                public int ReadTimeOut = System.Threading.Timeout.Infinite;     //初期値は無限に待機

            }

            #region Property

            private Param _param;
            /// <summary>
            /// セッティングを保持する
            /// </summary>
            public Param param
            {
                get => this._param;
                set
                {
                    if (value.encoding != this._param.encoding)
                    {
                        this._encoding = Encoding.GetEncoding(value.encoding);
                    }
                    this._param = value;
                }
            }

            /// <summary>
            /// 送信先のIPアドレス
            /// </summary>
            public string IP
            {
                get => _param.IP;
                set => _param.IP = value;
            }

            /// <summary>
            /// 送信先のPortNo
            /// </summary>
            public int SendPort
            {
                
                get => this._param.SendPort;
                set
                {
                    this._param.SendPort = value;
                    if (this._Client.Client.RemoteEndPoint != null)
                    {
                        ((System.Net.IPEndPoint)this._Client.Client.RemoteEndPoint).Port = value;
                    }
                }
            }

            /// <summary>
            /// 受信時に終端とみなす改行文字
            /// </summary>
            public string Res_EndCode
            {
                get { return this._param.Res_EndCode; }
                set
                {
                    if (value == "\r\n" || value == "\r" || value == "\n")
                    {
                        this._param.Res_EndCode = value;
                    }
                }
            }

            /// <summary>
            /// 送信時に文字列末につける終端文字
            /// </summary>
            public string Send_EndCode
            {
                get { return this._param.Send_EndCode; }
                set
                {
                    if (value == "\r\n" || value == "\r" || value == "\n")
                    {
                        this._param.Send_EndCode = value;
                    }
                }
            }
            
            /// <summary>
            /// 送信タイムアウトを設定します [ミリ秒]
            /// </summary>
            public int WriteTimeOut
            {
                get => this._param.WriteTimeOut;
                set
                {
                    if (this.ns != null)
                    {
                        this.ns.WriteTimeout    = value;
                    }

                    this._param.WriteTimeOut = value;
                }
            }

            /// <summary>
            /// 受信タイムアウトを設定します [ミリ秒]
            /// </summary>
            public int ReadTimeOut
            {
                get => this._param.ReadTimeOut;
                set
                {
                    if (this.ns != null)
                    {
                        this.ns.ReadTimeout = value;
                    }

                    this._param.ReadTimeOut = value;
                }
            }

            /// <summary>
            /// 接続状態であるかどうか
            /// </summary>
            public bool IsConnect
            {
                get => (this.ns != null);
            }

            private Encoding _encoding = Encoding.UTF8;
            /// <summary>
            /// 送受信で使用するエンコーディング
            /// </summary>
            public string _Encoding
            {
                get => this._param.encoding;
                set
                {
                    this._encoding = Encoding.GetEncoding(value);
                    this._param.encoding = value;
                }
            }

            #endregion

            private TcpClient _Client;            
            
            


            /// <summary>
            /// 受信PortNoを取得します
            /// </summary>
            public int RecivePort
            {
                get => this._param.RecivePort;
                set
                {
                    if (this._Client.Client.LocalEndPoint != null)
                    {
                        ((System.Net.IPEndPoint)this._Client.Client.LocalEndPoint).Port = value;
                    }
                    this._param.RecivePort = value;
                }
            }

            private System.Net.Sockets.NetworkStream ns;

            public Client(string ip = Const.LocalHost, int port = Const.DefaultPort)
            {
                this._Client = new TcpClient();
                this._param = new Param();
                this.IP = ip;
                this.SendPort = port;
            }

            public Client(int port)
            {
                this._Client = new TcpClient();
                this._param = new Param();
                this.IP = Const.LocalHost;
                this.SendPort = port;
            }

            public Client(string ip)
            {
                this._Client = new TcpClient();
                this._param = new Param();
                this.IP = ip;
                this.SendPort = Const.DefaultPort;
                this.SendPort = Const.DefaultPort;
            }

            ~Client()
            {
                if (this._Client != null)
                {
                    this._Client.Close();
                }

            }

            public void Connect()
            {
                try
                {
                    this._Client = new TcpClient(this.IP, this.SendPort);
                    ns = this._Client.GetStream();
                }
                catch (Exception err)
                {
                    throw;                    
                }
                 

                // TimeOut 設定            
                ns.ReadTimeout  = this.param.ReadTimeOut;
                ns.WriteTimeout = this.param.WriteTimeOut;
            }

            /// <summary>
            /// 通信を切断する
            /// </summary>
            public void DisConnect()
            {
                ns.Close();
                ns.Dispose();
            }

            /// <summary>
            /// 文字列を送信し、その応答コードを返す
            /// </summary>
            /// <param name="mes"></param>
            /// <returns></returns>
            public string Send(string mes)
            {

                // String → byte[] へエンコーディング
                byte[] sendBytes = this._encoding.GetBytes(mes + this._param.Send_EndCode);

                try
                {
                    // Write
                    ns.Write(sendBytes, 0, sendBytes.Length);
                }
                catch (Exception)
                {
                    throw;
                }
                
                string resMes;
                // 返信受信
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    byte[] readBytes = new byte[256];
                    int resSize = 0;

                    // 終端文字がくるまで読み込み

                    char endChar = this._param.Res_EndCode[this._param.Res_EndCode.Length - 1];

                    try
                    {
                        do
                        {
                            resSize = ns.Read(readBytes, 0, readBytes.Length);

                            if (resSize == 0) // 読み取れたbyteサイズが 0 なら
                            {
                                break;
                            }

                            // 読み込めた内容をメモリーストリームへ格納
                            ms.Write(readBytes, 0, resSize);

                        } while (ns.DataAvailable || readBytes[resSize - 1] != endChar); // まだ読み取れるデータがある  ||  データの最後が終端文字ではない場合   続行

                        resMes = this._encoding.GetString(ms.GetBuffer(), 0, (int)ms.Length);
                    }
                    catch (Exception)
                    {
                        throw;
                    }                             
                    
                }                             
                
                return resMes;
            }
        }

        public delegate string ReciveFunction(string message, ref bool endFlag);

        public class Server
        {
            private string _OwnIP;
            public int _OwnPort;
            private TcpListener _listener;

            private string _Res_EndCode = "\r\n";
            /// <summary>
            /// 受信時に終端とみなす改行文字
            /// </summary>
            public string Res_EndCode
            {
                get { return this._Res_EndCode; }
                set
                {
                    if (value == "\r\n" || value == "\r" || value == "\n")
                    {
                        this._Res_EndCode = value;
                    }
                }
            }

            private string _Send_EndCode = "\r\n";
            /// <summary>
            /// 送信時に文字列末につける終端文字
            /// </summary>
            public string Send_EndCode
            {
                get { return this._Send_EndCode; }
                set
                {
                    if (value == "\r\n" || value == "\r" || value == "\n")
                    {
                        this._Send_EndCode = value;
                    }
                }
            }

            public Encoding encoding = Encoding.UTF8;

            public Server(string ip = "127.0.0.1", int port = 80)
            {
                this._OwnIP = ip;
                this._OwnPort = port;
                this._listener = new TcpListener(System.Net.IPAddress.Parse(ip), port);
            }


            public void ListenStart(ReciveFunction func, int timeout = System.Threading.Timeout.Infinite)
            {
                this._listener.Start();
                char endChar = this._Res_EndCode[this._Res_EndCode.Length - 1];

                //接続要求があったら受け入れる
                System.Net.Sockets.TcpClient client = this._listener.AcceptTcpClient();

                //NetworkStreamを取得
                System.Net.Sockets.NetworkStream ns = client.GetStream();

                // TimeOut 設定
                ns.ReadTimeout = timeout; // 10 Sec
                ns.WriteTimeout = timeout; // 10 Sec

                while (true)
                {
                    if (ns.CanRead == false || ns.CanWrite == false)
                    {
                        Console.WriteLine("Connect End");
                    }

                    //クライアントから送られたデータを受信する
                    bool disconnected = false;      //受信失敗フラグ
                    System.IO.MemoryStream ms = new System.IO.MemoryStream();
                    byte[] resBytes = new byte[256];
                    int resSize = 0;

                    bool endFlag = false;

                    // 受信内容取得
                    do
                    {
                        //データの一部を受信する
                        resSize = ns.Read(resBytes, 0, resBytes.Length);

                        if (resSize == 0) // 読み取れたbyteサイズが 0 なら
                        {
                            disconnected = true;
                            break;
                        }

                        // 読み込めた内容をメモリーストリームへ格納
                        ms.Write(resBytes, 0, resSize);


                    } while (ns.DataAvailable || resBytes[resSize - 1] != endChar);


                    //受信したデータを文字列に変換
                    string resMsg = this.encoding.GetString(ms.GetBuffer(), 0, (int)ms.Length);
                    ms.Close();

                    if (!disconnected)
                    {
                        //クライアントにデータを送信する
                        //クライアントに送信する文字列を作成
                        //string sendMsg = resMsg.Length.ToString();

                        string sendMsg = func(resMsg, ref endFlag);


                        //文字列をByte型配列に変換
                        byte[] sendBytes = this.encoding.GetBytes(sendMsg + this._Res_EndCode);

                        //データを送信する
                        ns.Write(sendBytes, 0, sendBytes.Length);
                    }

                    if (endFlag == true)
                    {
                        break;
                    }
                }


                //閉じる
                ns.Close();
                client.Close();
                this._listener.Stop();

            }
        }
    }
}
